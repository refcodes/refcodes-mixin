// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a filenames suffix property. Each filename
 * extensions also contains the dot "." separating the extension from the base
 * filename, e.g. ".ini".
 */
public interface FilenameSuffixesAccessor {

	/**
	 * Retrieves the filenames extension from the filenames extension property.
	 * Each filename extensions also contains the dot "." separating the
	 * extension from the name, e.g. ".ini".
	 * 
	 * @return The filenames extension stored by the filenames extension
	 *         property.
	 */
	String[] getFilenameSuffixes();

	/**
	 * Provides a mutator for a filenames extension property.
	 */
	public interface FilenameSuffixesMutator {

		/**
		 * Sets the filenames extension for the filenames extension property.
		 * Each filename extensions also contains the dot "." separating the
		 * extension from the filename, e.g. ".ini".
		 * 
		 * @param aFilenameSuffixes The filenames extension to be stored by the
		 *        filenames extension property.
		 */
		void setFilenameSuffixes( String[] aFilenameSuffixes );
	}

	/**
	 * Provides a builder method for a filenames extension property returning
	 * the builder for applying multiple build operations. Each filename
	 * extensions also contains the dot "." separating the extension from the
	 * name, e.g. ".ini".
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FilenameSuffixesBuilder<B extends FilenameSuffixesBuilder<B>> {

		/**
		 * Sets the filenames extension for the filenames extension property.
		 * Each filename extensions also contains the dot "." separating the
		 * extension from the filename, e.g. ".ini".
		 * 
		 * @param aFilenameSuffixes The filenames extension to be stored by the
		 *        filenames extension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFilenameSuffixes( String[] aFilenameSuffixes );
	}

	/**
	 * Provides a filenames extension property. Each filename extensions also
	 * contains the dot "." separating the extension from the filename, e.g.
	 * ".ini".
	 */
	public interface FilenameSuffixesProperty extends FilenameSuffixesAccessor, FilenameSuffixesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} array
		 * (setter) as of {@link #setFilenameSuffixes(String[])} and returns the
		 * very same value (getter).
		 * 
		 * @param aFilenameSuffixes The {@link String} array to set (via
		 *        {@link #setFilenameSuffixes(String[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String[] letFilenameSuffixes( String[] aFilenameSuffixes ) {
			setFilenameSuffixes( aFilenameSuffixes );
			return aFilenameSuffixes;
		}
	}
}
