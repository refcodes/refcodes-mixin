// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a retries count property. A retry count is the
 * current count of retries being initiated.
 */
public interface RetryCountAccessor {

	/**
	 * Retrieves the count of retries from the retries count property. A retry
	 * count is the current count of retries being initiated.
	 * 
	 * @return The count of retries stored by the retries count property.
	 */
	int getRetryCount();

	/**
	 * Provides a mutator for a retries count property. A retry count is the
	 * current count of retries being initiated.
	 */
	public interface RetryCountMutator {

		/**
		 * Sets the count of retries for the retries count property. A retry
		 * count is the current count of retries being initiated.
		 * 
		 * @param aRetryCount The count of retries to be stored by the count of
		 *        retries count property.
		 */
		void setRetryCount( int aRetryCount );
	}

	/**
	 * Provides a builder method for a retries count property returning the
	 * builder for applying multiple build operations. A retry count is the
	 * current count of retries being initiated.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RetryCountBuilder<B extends RetryCountBuilder<B>> {

		/**
		 * Sets the count of retries for the retries count property. A retry
		 * count is the current count of retries being initiated.
		 * 
		 * @param aRetryCount The count of retries to be stored by the count of
		 *        retries count property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRetryCount( int aRetryCount );
	}

	/**
	 * Provides a retries count property.A retry count is the current count of
	 * retries being initiated.
	 */
	public interface RetryCountProperty extends RetryCountAccessor, RetryCountMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setRetryCount(int)} and returns the very same value (getter).
		 * A retry count is the current count of retries being initiated.
		 * 
		 * @param aRetries The integer to set (via {@link #setRetryCount(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letRetryCount( int aRetries ) {
			setRetryCount( aRetries );
			return aRetries;
		}
	}
}
