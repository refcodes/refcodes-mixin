// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a trim property.
 */
public interface TrimAccessor {

	/**
	 * Retrieves the trim status from the trim property.
	 * 
	 * @return The trim status stored by the trim property.
	 */
	boolean isTrim();

	/**
	 * Provides a mutator for a trim property.
	 */
	public interface TrimMutator {

		/**
		 * Sets the trim status for the trim property.
		 * 
		 * @param isTrim The trim status to be stored by the trim property.
		 */
		void setTrim( boolean isTrim );
	}

	/**
	 * Provides a builder for a trim property.
	 *
	 * @param <B> the generic type
	 */
	public interface TrimBuilder<B extends TrimBuilder<B>> extends TrimMutator {

		/**
		 * Builder method for the {@link #setTrim(boolean)} method.
		 * 
		 * @param isTrim The trim status to be stored by the trim property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withTrim( boolean isTrim );
	}

	/**
	 * Provides a trim property.
	 */
	public interface TrimProperty extends TrimAccessor, TrimMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setTrim(boolean)} and returns the very same value (getter).
		 * 
		 * @param isTrim The boolean to set (via {@link #setTrim(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letTrim( boolean isTrim ) {
			setTrim( isTrim );
			return isTrim;
		}
	}
}
