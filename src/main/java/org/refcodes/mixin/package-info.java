// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * A mixin from the point of view of this artifact represents a snippet of
 * functionality which can be mixed into a type. A mixin is not intended to be
 * used as a type by itself. A mixin enables you to unify your types by always
 * mixing the same mixin for the same functionality into your different types.
 * For example methods such as accessors (such as getters) for common purposes
 * should be named always the same to get high recognition factor by developers
 * using your API: This helps using your API much more intuitively. Such common
 * methods are defined in mixins in the <code>mixins</code>.
 */
package org.refcodes.mixin;
