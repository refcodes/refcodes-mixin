// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a UID property.
 */
public interface UniversalIdAccessor {

	/**
	 * Retrieves the UID from the UID property.
	 * 
	 * @return The UID stored by the UID property.
	 */
	String getUniversalId();

	/**
	 * Provides a mutator for a UID property.
	 */
	public interface UniversalIdMutator {

		/**
		 * Sets the UID for the UID property.
		 * 
		 * @param aUid The UID to be stored by the UID property.
		 */
		void setUniversalId( String aUid );
	}

	/**
	 * Provides a builder method for a Universal-TID property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface UniversalIdBuilder<B extends UniversalIdBuilder<B>> {

		/**
		 * Sets the Universal-TID for the Universal-TID property.
		 * 
		 * @param aUid The Universal-TID to be stored by the Universal-TID
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withUniversalId( String aUid );
	}

	/**
	 * Provides a UID property.
	 */
	public interface UniversalIdProperty extends UniversalIdAccessor, UniversalIdMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setUniversalId(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aUniversalId The {@link String} to set (via
		 *        {@link #setUniversalId(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letUniversalId( String aUniversalId ) {
			setUniversalId( aUniversalId );
			return aUniversalId;
		}
	}
}
