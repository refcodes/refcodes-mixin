// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a magic number property.
 *
 * @param <T> The type of the magic number to be accessed.
 */
public interface MagicNumberAccessor<T> {

	/**
	 * Retrieves the magic number from the magic number property.
	 * 
	 * @return The magic number stored by the magic number property.
	 */
	T getMagicNumber();

	/**
	 * Provides a mutator for a magic number property.
	 * 
	 * @param <T> The type of the magic number to be accessed.
	 */
	public interface MagicNumberMutator<T> {

		/**
		 * Sets the magic number for the magic number property.
		 * 
		 * @param aMagicNumber The magic number to be stored by the magic number
		 *        property.
		 */
		void setMagicNumber( T aMagicNumber );
	}

	/**
	 * Provides a builder method for a magic number property returning the
	 * builder for applying multiple build operations.
	 *
	 * @param <T> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MagicNumberBuilder<T, B extends MagicNumberBuilder<T, B>> {

		/**
		 * Sets the magic number for the magic number property.
		 * 
		 * @param aMagicNumber The magic number to be stored by the magic number
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMagicNumberentity( T aMagicNumber );
	}

	/**
	 * Provides a magic number property.
	 * 
	 * @param <T> The type of the magic number to be accessed.
	 */
	public interface MagicNumberProperty<T> extends MagicNumberAccessor<T>, MagicNumberMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setMagicNumber(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aMagicNumber The value to set (via
		 *        {@link #setMagicNumber(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letMagicNumber( T aMagicNumber ) {
			setMagicNumber( aMagicNumber );
			return aMagicNumber;
		}
	}
}
