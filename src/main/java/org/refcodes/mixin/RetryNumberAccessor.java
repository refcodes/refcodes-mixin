// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a retry number. A retry number is the overall number
 * of retries to use when counting retries.
 */
public interface RetryNumberAccessor {

	/**
	 * Retrieves the number of retries from the retry number. A retry number is
	 * the overall number of retries to use when counting retries.
	 * 
	 * @return The number of retries stored by the retry number.
	 */
	int getRetryNumber();

	/**
	 * Provides a mutator for a retry number. A retry number is the overall
	 * number of retries to use when counting retries.
	 */
	public interface RetryNumberMutator {

		/**
		 * Sets the number of retries for the retry number. A retry number is
		 * the overall number of retries to use when counting retries.
		 * 
		 * @param aRetryNumber The number of retries to be stored by the number
		 *        of retry number.
		 */
		void setRetryNumber( int aRetryNumber );
	}

	/**
	 * Provides a builder method for a retry number returning the builder for
	 * applying multiple build operations. A retry number is the overall number
	 * of retries to use when counting retries.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RetryNumberBuilder<B extends RetryNumberBuilder<B>> {

		/**
		 * Sets the number of retries for the retry number. A retry number is
		 * the overall number of retries to use when counting retries.
		 * 
		 * @param aRetryNumber The number of retries to be stored by the number
		 *        of retry number.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRetryNumber( int aRetryNumber );
	}

	/**
	 * Provides a retry number.A retry number is the overall number of retries
	 * to use when counting retries.
	 */
	public interface RetryNumberProperty extends RetryNumberAccessor, RetryNumberMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setRetryNumber(int)} and returns the very same value
		 * (getter). A retry number is the overall number of retries to use when
		 * counting retries.
		 * 
		 * @param aRetries The integer to set (via
		 *        {@link #setRetryNumber(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letRetryNumber( int aRetries ) {
			setRetryNumber( aRetries );
			return aRetries;
		}
	}
}
