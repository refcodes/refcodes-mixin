// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a ID property.
 *
 * @param <T> The type of the ID to be accessed.
 */
public interface IdAccessor<T> {

	/**
	 * Retrieves the ID from the ID property.
	 * 
	 * @return The ID stored by the ID property.
	 */
	T getId();

	/**
	 * Provides a mutator for a ID property.
	 * 
	 * @param <T> The type of the ID to be accessed.
	 */
	public interface IdMutator<T> {

		/**
		 * Sets the ID for the ID property.
		 * 
		 * @param aId The ID to be stored by the ID property.
		 */
		void setId( T aId );
	}

	/**
	 * Provides a builder method for a ID property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <T> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface IdBuilder<T, B extends IdBuilder<T, B>> {

		/**
		 * Sets the ID for the ID property.
		 * 
		 * @param aId The ID to be stored by the ID property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withIdentity( T aId );
	}

	/**
	 * Provides a ID property.
	 * 
	 * @param <T> The type of the ID to be accessed.
	 */
	public interface IdProperty<T> extends IdAccessor<T>, IdMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setId(Object)} and returns the very same value (getter).
		 * 
		 * @param aId The value to set (via {@link #setId(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letId( T aId ) {
			setId( aId );
			return aId;
		}
	}
}
