// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a key property for e.g. key/value-pair.
 *
 * @param <K> The type of the key to be used.
 */
public interface KeyAccessor<K> {

	/**
	 * Retrieves the key from the key property.
	 * 
	 * @return The key stored by the key property.
	 */
	K getKey();

	/**
	 * Provides a mutator for a key property for e.g. key/value-pair.
	 * 
	 * @param <K> The type of the key property.
	 */
	public interface KeyMutator<K> {

		/**
		 * Sets the key for the key property.
		 * 
		 * @param aKey The key to be stored by the key property.
		 */
		void setKey( K aKey );

	}

	/**
	 * Provides a builder method for a key property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <K> the key type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface KeyBuilder<K, B extends KeyBuilder<K, B>> {

		/**
		 * Sets the key for the key property.
		 * 
		 * @param aKey The key to be stored by the key property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withKey( K aKey );
	}

	/**
	 * Provides a key property for e.g. key/value-pair.
	 * 
	 * @param <K> The type of the key property.
	 */
	public interface KeyProperty<K> extends KeyAccessor<K>, KeyMutator<K> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setKey(Object)} and returns the very same value (getter).
		 * 
		 * @param aKey The value to set (via {@link #setKey(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default K letKey( K aKey ) {
			setKey( aKey );
			return aKey;
		}
	}
}
