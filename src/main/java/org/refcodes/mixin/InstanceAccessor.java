// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a instance property.
 * 
 * @param <T> The generic instance type.
 */
public interface InstanceAccessor<T> {

	/**
	 * Retrieves the instance property.
	 * 
	 * @return The instance stored by the property.
	 */
	T getInstance();

	/**
	 * Provides a mutator for a instance property.
	 * 
	 * @param <T> The generic instance type.
	 */
	public interface InstanceMutator<T> {

		/**
		 * Sets the instance for the property.
		 * 
		 * @param aInstance The instance to be stored by the property.
		 */
		void setInstance( T aInstance );
	}

	/**
	 * Provides a builder method for a instance property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <T> The generic instance type.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface InstanceBuilder<T, B extends InstanceBuilder<T, B>> {

		/**
		 * Sets the instance for the instance property.
		 * 
		 * @param aInstance The instance to be stored by the instance property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withInstance( T aInstance );
	}

	/**
	 * Provides a instance property.
	 * 
	 * @param <T> The generic instance type.
	 */
	public interface InstanceProperty<T> extends InstanceAccessor<T>, InstanceMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setInstance(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aInstance The value to set (via {@link #setInstance(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letInstance( T aInstance ) {
			setInstance( aInstance );
			return aInstance;
		}
	}
}
