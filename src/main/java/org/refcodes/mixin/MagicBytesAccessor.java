// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.nio.charset.Charset;

/**
 * Provides an accessor for a magic bytes property.
 */
public interface MagicBytesAccessor {

	/**
	 * Retrieves the magic bytes from the magic bytes property.
	 * 
	 * @return The magic bytes stored by the magic bytes property.
	 */
	byte[] getMagicBytes();

	/**
	 * Provides a mutator for a magic bytes property.
	 */
	public interface MagicBytesMutator {

		/**
		 * Sets the magic bytes for the magic bytes property.
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 */
		void setMagicBytes( byte[] aMagicBytes );

		/**
		 * Sets the magic bytes for the magic bytes property. If not stated
		 * otherwise, the {@link String} is converted using the platform
		 * encoding into an array of bytes.
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 */
		default void setMagicBytes( String aMagicBytes ) {
			setMagicBytes( aMagicBytes.getBytes() );
		}

		/**
		 * Sets the magic bytes for the magic bytes property.
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 */
		default void setMagicBytes( String aMagicBytes, Charset aEncoding ) {
			setMagicBytes( aMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a builder method for a magic bytes property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MagicBytesBuilder<B extends MagicBytesBuilder<B>> {

		/**
		 * Sets the magic bytes for the magic bytes property.
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMagicBytes( byte[] aMagicBytes );

		/**
		 * Sets the magic bytes for the magic bytes property. If not stated
		 * otherwise, the {@link String} is converted using the platform
		 * encoding into an array of bytes.
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withMagicBytes( String aMagicBytes ) {
			return withMagicBytes( aMagicBytes.getBytes() );
		}

		/**
		 * Sets the magic bytes for the magic bytes property.
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withMagicBytes( String aMagicBytes, Charset aEncoding ) {
			return withMagicBytes( aMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a magic bytes property.
	 */
	public interface MagicBytesProperty extends MagicBytesAccessor, MagicBytesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #setMagicBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aMagicBytes The byte array to set (via
		 *        {@link #setMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letMagicBytes( byte[] aMagicBytes ) {
			setMagicBytes( aMagicBytes );
			return aMagicBytes;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #setMagicBytes(byte[])} and returns the very same value
		 * (getter). If not stated otherwise, the {@link String} is converted
		 * using the platform encoding into an array of bytes.
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letMagicBytes( String aMagicBytes ) {
			setMagicBytes( aMagicBytes );
			return aMagicBytes;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #setMagicBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aMagicBytes The magic bytes to be stored by the magic bytes
		 *        property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letMagicBytes( String aMagicBytes, Charset aEncoding ) {
			setMagicBytes( aMagicBytes, aEncoding );
			return aMagicBytes;
		}
	}
}
