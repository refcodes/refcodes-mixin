// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a path property.
 */
public interface PathAccessor {

	/**
	 * Retrieves the path from the path property.
	 * 
	 * @return The path stored by the path property.
	 */
	String getPath();

	/**
	 * Provides a mutator for a path property.
	 */
	public interface PathMutator {

		/**
		 * Sets the path for the path property.
		 * 
		 * @param aPath The path to be stored by the path property.
		 */
		void setPath( String aPath );
	}

	/**
	 * Provides a mutator for an path property.
	 * 
	 * @param <B> The builder which implements the {@link PathBuilder}.
	 */
	public interface PathBuilder<B extends PathBuilder<?>> {

		/**
		 * Sets the path to use and returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aPath The path to be stored by the path property.
		 * 
		 * @return This {@link PathBuilder} instance to continue configuration.
		 */
		B withPath( String aPath );
	}

	/**
	 * Provides a path property.
	 */
	public interface PathProperty extends PathAccessor, PathMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setPath(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPath The {@link String} to set (via
		 *        {@link #setPath(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPath( String aPath ) {
			setPath( aPath );
			return aPath;
		}
	}
}
