// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a Escape-Code property.
 */
public interface EscapeCodesStatusAccessor {

	/**
	 * Retrieves the Escape-Code status from the Escape-Code property.
	 * 
	 * @return The Escape-Code status stored by the Escape-Code property.
	 */
	boolean isEscapeCodesEnabled();

	/**
	 * Provides a mutator for a Escape-Code property.
	 */
	public interface EscapeCodeStatusMutator {

		/**
		 * Sets the Escape-Code status for the Escape-Code property.
		 * 
		 * @param isEscCodesEnabled The Escape-Code status to be stored by the
		 *        Escape-Code property.
		 */
		void setEscapeCodesEnabled( boolean isEscCodesEnabled );
	}

	/**
	 * Provides a builder for a Escape-Code property.
	 *
	 * @param <B> the generic type
	 */
	public interface EscapeCodeStatusBuilder<B extends EscapeCodeStatusBuilder<B>> {

		/**
		 * Builder method for the Escape-Code property.
		 * 
		 * @param isEscCodesEnabled The Escape-Code status to be stored by the
		 *        Escape-Code property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withEscapeCodesEnabled( boolean isEscCodesEnabled );
	}

	/**
	 * Provides a Escape-Code property.
	 */
	public interface EscapeCodeStatusProperty extends EscapeCodesStatusAccessor, EscapeCodeStatusMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setEscapeCodesEnabled(boolean)} and returns the very same
		 * value (getter).
		 * 
		 * @param isEscCodesEnabled The boolean to set (via
		 *        {@link #setEscapeCodesEnabled(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letEscapeCodesEnabled( boolean isEscCodesEnabled ) {
			setEscapeCodesEnabled( isEscCodesEnabled );
			return isEscCodesEnabled;
		}
	}
}
