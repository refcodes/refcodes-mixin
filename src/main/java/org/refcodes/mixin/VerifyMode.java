// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * The verify-mode determines (when supported) whether an algorithm verifies its
 * result before proceeding. In case verification fails, either the problem is
 * just reported {@link #REPORT_ONLY} e.g. logged or an exception (such as the
 * VerifyRuntimeException found in the refcodes-exception artifact) is thrown (
 * {@link #THROW_EXCEPTION}).
 */
public enum VerifyMode {
	/**
	 * Verification is disabled.
	 */
	NONE(false),
	/**
	 * Verification is enabled, problems are only reported.
	 */
	REPORT_ONLY(true),
	/**
	 * Verification is enabled, problems cause an exception. You may use the
	 * VerifyRuntimeException found in the refcodes-exception artifact.
	 */
	THROW_EXCEPTION(true);

	private boolean _isVerify;

	/**
	 * Instantiates a new verify mode.
	 *
	 * @param isVerify the is verify
	 */
	private VerifyMode( boolean isVerify ) {
		_isVerify = isVerify;
	}

	/**
	 * Checks if is verify.
	 *
	 * @return true, if is verify
	 */
	public boolean isVerify() {
		return _isVerify;
	}
}
