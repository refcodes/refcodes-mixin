// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a enabled property.
 */
public interface EnabledAccessor {

	/**
	 * Retrieves the enabled status from the enabled property.
	 * 
	 * @return The enabled status stored by the enabled property.
	 */
	boolean isEnabled();

	/**
	 * Provides a mutator for a enabled property.
	 */
	public interface EnabledMutator {

		/**
		 * Sets the enabled status for the enabled property.
		 * 
		 * @param isEnabled The enabled status to be stored by the enabled
		 *        property.
		 */
		void setEnabled( boolean isEnabled );
	}

	/**
	 * Provides a builder for a enabled property.
	 *
	 * @param <B> the generic type
	 */
	public interface EnabledBuilder<B extends EnabledBuilder<B>> {

		/**
		 * Builder method for the {@link EnabledMutator#setEnabled(boolean)}
		 * method.
		 * 
		 * @param isEnabled The enabled status to be stored by the enabled
		 *        property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withEnabled( boolean isEnabled );
	}

	/**
	 * Provides a enabled property.
	 */
	public interface EnabledProperty extends EnabledAccessor, EnabledMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setEnabled(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isEnabled The boolean to set (via
		 *        {@link #setEnabled(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letEnabled( boolean isEnabled ) {
			setEnabled( isEnabled );
			return isEnabled;
		}
	}
}
