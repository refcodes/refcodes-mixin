// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a category property.
 * 
 * @param <T> The type of the category.
 */
public interface CategoryAccessor<T> {

	/**
	 * Retrieves the category from the category property.
	 * 
	 * @return The category stored by the category property.
	 */
	T getCategory();

	/**
	 * Provides a mutator for a category property.
	 * 
	 * @param <T> The type of the category.
	 */
	public interface CategoryMutator<T> {

		/**
		 * Sets the category for the category property.
		 * 
		 * @param aCategory The category to be stored by the category property.
		 */
		void setCategory( T aCategory );
	}

	/**
	 * Provides a builder method for a category property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <T> The type of the category.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CategoryBuilder<T, B extends CategoryBuilder<T, B>> {

		/**
		 * Sets the category for the category property.
		 * 
		 * @param aCategory The category to be stored by the category property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCategory( T aCategory );
	}

	/**
	 * Provides a category property.
	 * 
	 * @param <T> The type of the category.
	 */
	public interface CategoryProperty<T> extends CategoryAccessor<T>, CategoryMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setCategory(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aCategory The value to set (via {@link #setCategory(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letCategory( T aCategory ) {
			setCategory( aCategory );
			return aCategory;
		}
	}
}
