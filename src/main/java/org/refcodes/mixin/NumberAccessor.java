// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a number property for e.g. key / number pair.
 *
 * @param <V> The type of the number to be used.
 */
public interface NumberAccessor<V> {

	/**
	 * Retrieves the number from the number property.
	 * 
	 * @return The number stored by the number property.
	 */
	V getNumber();

	/**
	 * Extends the {@link NumberAccessor} with a setter method.
	 * 
	 * @param <V> The type of the number property.
	 */
	public interface NumberMutator<V> {

		/**
		 * Sets the number for the number property.
		 * 
		 * @param aNumber The number to be stored by the number property.
		 */
		void setNumber( V aNumber );

	}

	/**
	 * Provides a builder method for a the property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <V> the value type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface NumberBuilder<V, B extends NumberBuilder<V, B>> {

		/**
		 * Sets the number for the the property.
		 * 
		 * @param aNumber The number to be stored by the number property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withNumber( V aNumber );
	}

	/**
	 * Extends the {@link NumberAccessor} with a setter method.
	 * 
	 * @param <V> The type of the number property.
	 */
	public interface NumberProperty<V> extends NumberAccessor<V>, NumberMutator<V> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setNumber(Object)} and returns the very same value (getter).
		 * 
		 * @param aNumber The value to set (via {@link #setNumber(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default V letNumber( V aNumber ) {
			setNumber( aNumber );
			return aNumber;
		}
	}
}
