// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a result property for e.g. key / result pair.
 *
 * @param <RES> The type of the result to be used.
 * @param <EXC> The exception thrown in case no result is available. Use
 *        {@link RuntimeException} to prevent forcing a try/catch block.
 */
public interface ResultAccessor<RES, EXC extends Exception> {

	/**
	 * Retrieves the result from the result property. This method may block or
	 * throw an exception in case there is none such result. Use
	 * {@link #hasResult()} to test beforehand whether we already have a result.
	 * 
	 * @return The result stored by the result property.
	 * 
	 * @throws EXC Thrown in case an exception occurred instead of the expected
	 *         result.
	 */
	RES getResult() throws EXC;

	/**
	 * Peeks for the result from the result property: In case there is a result,
	 * then this result is thrown, in case there was an exception, then the
	 * exception is thrown. In case there is no result (exception) yet
	 * available, this method returns null! This method does not block in case
	 * there is none such result. Use {@link #hasResult()} to test beforehand
	 * whether we already have a result or {@link #getResult()} for a blocking
	 * variant of this method. NOTE: A return value of null does not mean that
	 * there is no result as the result might be null!
	 *
	 * @return The result stored by the result property or null if there is no
	 *         such result yet.
	 */
	// RES peekResult() throws EXC;

	/**
	 * Tests whether there is already result. This method is
	 * https://www.metacodes.pro as it must not use {@link #getResult()} to test
	 * whether there is a result as {@link #getResult()} may block till we have
	 * a result or throw an exception in case there is none such result.
	 * 
	 * @return True in case there is a HTTP-Response, else false.
	 */
	boolean hasResult();

	/**
	 * Waits for the result. This method may block in case there is none such
	 * result or an exception has been thrown. Use {@link #hasResult()} to test
	 * beforehand whether we already have a result. The default implementation
	 * just calls {@link #getResult()} (ignoring the return value or any thrown
	 * exception), depending on the actual implementation, this method should be
	 * overridden in case calling {@link #getResult()} causes resource overhead
	 * or doesn't block.
	 */
	default void waitForResult() {
		try {
			getResult();
		}
		catch ( Exception ignore ) {}
	}

	/**
	 * Waits for the result. This method may block till the timeout has run out
	 * and/or throw an exception in case there is none such result. Use
	 * {@link #hasResult()} to test beforehand whether we already have a result.
	 * The default implementation just calls {@link #getResult()} (ignoring the
	 * return value), depending on the actual implementation, this method should
	 * be overridden in case calling {@link #getResult()} causes resource
	 * overhead or doesn't block.
	 *
	 * @param <RES> the generic type
	 */
	// See IoResultAccessor |-->
	//	default void waitForResult( long aTimeoutMillis ) throws InterruptedIOException, EXC {
	//		long theStartTime = System.currentTimeMillis();
	//		while ( !hasResult() && System.currentTimeMillis() - theStartTime < aTimeoutMillis ) {
	//			try {
	//				Thread.sleep( aTimeoutMillis / 100 );
	//			}
	//			catch ( InterruptedException ignore ) {}
	//		}
	//		if ( !hasResult() ) {
	//			throw new InterruptedIOException( "Unable to retrieve a result after a timeout of <" + aTimeoutMillis + "> milliseconds." );
	//		}
	//		getResult();
	//	}
	// See IoResultAccessor <--|

	/**
	 * Extends the {@link ResultAccessor} with a setter method.
	 * 
	 * @param <RES> The type of the result property.
	 */
	public interface ResultMutator<RES> {

		/**
		 * Sets the result for the result property.
		 * 
		 * @param aResult The result to be stored by the result property.
		 */
		void setResult( RES aResult );

	}

	/**
	 * Provides a builder method for a result property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <RES> The type of the result to be used.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ResultBuilder<RES, B extends ResultBuilder<RES, B>> {

		/**
		 * Sets the result for the result property.
		 * 
		 * @param aResult The result to be stored by the result property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withResult( RES aResult );
	}

	/**
	 * Extends the {@link ResultAccessor} with a setter method.
	 * 
	 * @param <RES> The type of the result property.
	 * @param <EXC> The exception thrown in case no result is available. Use
	 *        {@link RuntimeException} to prevent forcing a try/catch block.
	 */
	public interface ResultProperty<RES, EXC extends Exception> extends ResultAccessor<RES, EXC>, ResultMutator<RES> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setResult(Object)} and returns the very same value (getter).
		 * 
		 * @param aResult The value to set (via {@link #setResult(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default RES letResult( RES aResult ) {
			setResult( aResult );
			return aResult;
		}
	}
}
