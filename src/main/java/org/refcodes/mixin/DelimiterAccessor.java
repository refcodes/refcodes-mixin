// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a delimiter property.
 */
public interface DelimiterAccessor {

	/**
	 * Retrieves the delimiter from the delimiter property.
	 * 
	 * @return The delimiter stored by the delimiter property.
	 */
	char getDelimiter();

	/**
	 * Provides a mutator for a delimiter property.
	 */
	public interface DelimiterMutator {

		/**
		 * Sets the delimiter for the delimiter property.
		 * 
		 * @param aDelimiter The delimiter to be stored by the delimiter
		 *        property.
		 */
		void setDelimiter( char aDelimiter );
	}

	/**
	 * Provides a builder method for a delimiter property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DelimiterBuilder<B extends DelimiterBuilder<B>> {

		/**
		 * Sets the delimiter for the delimiter property.
		 * 
		 * @param aDelimiter The delimiter to be stored by the delimiter
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDelimiter( char aDelimiter );
	}

	/**
	 * Provides a delimiter property.
	 */
	public interface DelimiterProperty extends DelimiterAccessor, DelimiterMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setDelimiter(char)} and returns the very same value (getter).
		 * 
		 * @param aDelimiter The character to set (via
		 *        {@link #setDelimiter(char)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default char letDelimiter( char aDelimiter ) {
			setDelimiter( aDelimiter );
			return aDelimiter;
		}
	}
}
