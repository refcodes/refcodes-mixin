// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a row width property.
 */
public interface RowWidthAccessor {

	/**
	 * Gets the currently set width being used.
	 * 
	 * @return The currently configured row widths.
	 */
	int getRowWidth();

	/**
	 * Provides a mutator for an output stream property.
	 * 
	 * @param <B> The builder which implements the {@link RowWidthBuilder}.
	 */
	public interface RowWidthBuilder<B extends RowWidthBuilder<?>> {

		/**
		 * Sets the rows width to use returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aRowWidth The row width to be used when printing a row or the
		 *        header.
		 * 
		 * @return This instance to continue configuration.
		 */
		B withRowWidth( int aRowWidth );
	}

	/**
	 * Provides a mutator for an output stream property.
	 */
	public interface RowWidthMutator {

		/**
		 * Sets the width to be used.
		 * 
		 * @param aRowWidth The row width to be stored by the row width
		 *        property.
		 */
		void setRowWidth( int aRowWidth );
	}

	/**
	 * Provides a row width property.
	 */
	public interface RowWidthProperty extends RowWidthAccessor, RowWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setRowWidth(int)} and returns the very same value (getter).
		 * 
		 * @param aRowWidth The integer to set (via {@link #setRowWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letRowWidth( int aRowWidth ) {
			setRowWidth( aRowWidth );
			return aRowWidth;
		}
	}
}