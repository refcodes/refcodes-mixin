// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a prefix property.
 */
public interface PrefixAccessor {

	/**
	 * Retrieves the prefix from the prefix property.
	 * 
	 * @return The prefix stored by the prefix property.
	 */
	String getPrefix();

	/**
	 * Provides a mutator for a prefix property.
	 */
	public interface PrefixMutator {

		/**
		 * Sets the prefix for the prefix property.
		 * 
		 * @param aPrefix The prefix to be stored by the prefix property.
		 */
		void setPrefix( String aPrefix );
	}

	/**
	 * Provides a builder method for a prefix property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PrefixBuilder<B extends PrefixBuilder<B>> {

		/**
		 * Sets the prefix for the prefix property.
		 * 
		 * @param aPrefix The prefix to be stored by the prefix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPrefix( String aPrefix );
	}

	/**
	 * Provides a prefix property.
	 */
	public interface PrefixProperty extends PrefixAccessor, PrefixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setPrefix(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPrefix The {@link String} to set (via
		 *        {@link #setPrefix(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPrefix( String aPrefix ) {
			setPrefix( aPrefix );
			return aPrefix;
		}
	}
}
