// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a statusCode property for e.g. key / statusCode pair.
 *
 * @param <S> The type of the state to be used.
 */
public interface StatusAccessor<S> {

	/**
	 * Retrieves the state from the statusCode property.
	 * 
	 * @return The statusCode stored by the state property.
	 */
	S getStatus();

	/**
	 * Extends the {@link StatusAccessor} with a setter method.
	 * 
	 * @param <S> The type of the statusCode property.
	 */
	public interface StatusMutator<S> {

		/**
		 * Sets the statusCode for the state property.
		 * 
		 * @param aState The state to be stored by the state property.
		 */
		void setStatus( S aState );

	}

	/**
	 * Provides a builder method for a state property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <S> The type of the state to be used.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface StatusBuilder<S, B extends StatusBuilder<S, B>> {

		/**
		 * Sets the state for the state property.
		 * 
		 * @param aState The state to be stored by the state property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withStatus( S aState );
	}

	/**
	 * Extends the {@link StatusAccessor} with a setter method.
	 * 
	 * @param <S> The type of the state property.
	 */
	public interface StatusProperty<S> extends StatusAccessor<S>, StatusMutator<S> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setStatus(Object)} and returns the very same value (getter).
		 * 
		 * @param aStatus The value to set (via {@link #setStatus(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default S letStatus( S aStatus ) {
			setStatus( aStatus );
			return aStatus;
		}
	}
}
