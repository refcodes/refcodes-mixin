// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a channel property.
 */
public interface ChannelAccessor {

	/**
	 * Retrieves the channel from the channel property.
	 * 
	 * @return The channel stored by the channel property.
	 */
	String getChannel();

	/**
	 * Provides a mutator for a channel property.
	 */
	public interface ChannelMutator {

		/**
		 * Sets the channel for the channel property.
		 * 
		 * @param aChannel The channel to be stored by the channel property.
		 */
		void setChannel( String aChannel );
	}

	/**
	 * Provides a builder method for a channel property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ChannelBuilder<B extends ChannelBuilder<B>> {

		/**
		 * Sets the channel for the channel property.
		 * 
		 * @param aChannel The channel to be stored by the channel property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withChannel( String aChannel );
	}

	/**
	 * Provides a channel property.
	 */
	public interface ChannelProperty extends ChannelAccessor, ChannelMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setChannel(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aChannel The {@link String} to set (via
		 *        {@link #setChannel(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letChannel( String aChannel ) {
			setChannel( aChannel );
			return aChannel;
		}
	}
}
