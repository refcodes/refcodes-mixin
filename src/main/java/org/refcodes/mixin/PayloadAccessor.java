// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a payload property.
 * 
 * @param <P> The type of the payload to be carried.
 */
public interface PayloadAccessor<P> {

	/**
	 * Retrieves the payload from the payload property.
	 * 
	 * @return The payload stored by the payload property.
	 */
	P getPayload();

	/**
	 * Retrieves the value from the payload property. If there is no value for
	 * the property, then the provided value is returned. Depending on the
	 * implementation, not present might mean <code>null</code> or empty as well
	 * (being "" for a {@link String}).
	 * 
	 * @param aValue In case the value to be retrieved is null, then the given
	 *        value is returned.
	 * 
	 * @return The value stored by the payload property or the given value if
	 *         the stored value is null.
	 */
	default P getPayloadOr( P aValue ) {
		final P theValue = getPayload();
		if ( theValue == null || ( theValue instanceof String && ( (String) theValue ).isEmpty() ) ) {
			return aValue;
		}
		return theValue;
	}

	/**
	 * Provides a mutator for a payload property.
	 * 
	 * @param <P> The type of the payload to be carried.
	 */
	public interface PayloadMutator<P> {

		/**
		 * Sets the payload for the payload property.
		 * 
		 * @param aPayload The payload to be stored by the payload property.
		 */
		void setPayload( P aPayload );
	}

	/**
	 * Provides a builder method for a payload property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <P> The type of the payload to be carried.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PayloadBuilder<P, B extends PayloadBuilder<P, B>> {

		/**
		 * Sets the payload for the payload property.
		 * 
		 * @param aPayload The payload to be stored by the payload property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPayload( P aPayload );
	}

	/**
	 * Provides a payload property.
	 * 
	 * @param <P> The type of the payload to be carried.
	 */
	public interface PayloadProperty<P> extends PayloadAccessor<P>, PayloadMutator<P> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setPayload(Object)} and returns the very same value (getter).
		 * 
		 * @param aPayload The value to set (via {@link #setPayload(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default P letPayload( P aPayload ) {
			setPayload( aPayload );
			return aPayload;
		}
	}
}
