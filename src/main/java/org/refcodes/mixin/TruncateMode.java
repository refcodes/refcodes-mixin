// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * The {@link TruncateMode} specifies for according algorithms on how to
 * truncate a sequence.
 */
public enum TruncateMode {

	/**
	 * Truncate the head (left hand side) of a sequence.
	 */
	HEAD(true, false),

	/**
	 * Truncate both head and tail (left and right hand side) of a sequence.
	 */
	HEAD_AND_TAIL(true, true),

	/**
	 * Truncate the tail (right hand side) of a sequence.
	 */
	TAIL(false, true);

	private boolean _isHead;
	private boolean _isTail;

	private TruncateMode( boolean isHead, boolean isTail ) {
		_isHead = isHead;
		_isTail = isTail;
	}

	/**
	 * Returns true in case the head is to be truncated.
	 * 
	 * @return True in case the left hand side is to be truncated.
	 */
	public boolean isHead() {
		return _isHead;
	}

	/**
	 * Returns true in case the tail is to be truncated.
	 * 
	 * @return True in case the right hand side is to be truncated.
	 */
	public boolean isTail() {
		return _isTail;
	}
}
