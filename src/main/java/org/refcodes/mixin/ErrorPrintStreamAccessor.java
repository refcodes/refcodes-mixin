// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.io.PrintStream;

/**
 * Provides an accessor for an error {@link PrintStream} property.
 */
public interface ErrorPrintStreamAccessor {

	/**
	 * Retrieves the error {@link PrintStream} from the error
	 * {@link PrintStream} property.
	 * 
	 * @return The error {@link PrintStream} stored by the error
	 *         {@link PrintStream} property.
	 */
	PrintStream getErrorPrintStream();

	/**
	 * Provides a mutator for an error {@link PrintStream} property.
	 */
	public interface ErrorPrintStreamMutator {

		/**
		 * Sets the error {@link PrintStream} for the error {@link PrintStream}
		 * property.
		 * 
		 * @param aErrorPrintStream The error {@link PrintStream} to be stored
		 *        by the error {@link PrintStream} property.
		 */
		void setErrorPrintStream( PrintStream aErrorPrintStream );
	}

	/**
	 * Provides a mutator for an error {@link PrintStream} property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link ErrorPrintStreamBuilder}.
	 */
	public interface ErrorPrintStreamBuilder<B extends ErrorPrintStreamBuilder<?>> {

		/**
		 * Sets the error {@link PrintStream} to use and returns this builder as
		 * of the Builder-Pattern.
		 * 
		 * @param aErrorPrintStream The error {@link PrintStream} to be stored
		 *        by the error {@link PrintStream} property.
		 * 
		 * @return This {@link ErrorPrintStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withErrorPrintStream( PrintStream aErrorPrintStream );
	}

	/**
	 * Provides an error {@link PrintStream} property.
	 */
	public interface ErrorPrintStreamProperty extends ErrorPrintStreamAccessor, ErrorPrintStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link PrintStream}
		 * (setter) as of {@link #setErrorPrintStream(PrintStream)} and returns
		 * the very same value (getter).
		 * 
		 * @param aErrorPrintStream The {@link PrintStream} to set (via
		 *        {@link #setErrorPrintStream(PrintStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default PrintStream letErrorPrintStream( PrintStream aErrorPrintStream ) {
			setErrorPrintStream( aErrorPrintStream );
			return aErrorPrintStream;
		}
	}
}
