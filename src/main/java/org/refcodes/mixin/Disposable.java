// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Any type not being a mature component, which nevertheless can be destroyed,
 * implements this {@link Disposable} interface. This avoids the necessity to
 * depend on the refcodes-component artifact. In addition the semantic
 * distinction between a component in terms of refcodes-component and a plain
 * old java class or bean is clear.
 * <p>
 * ATTENTION: In case you intend to add support {@link #dispose()} functionality
 * to your mature components (in terms of refcodes-component), please implement
 * the <code>org.refcodes.component.Component</code> interface with its
 * <code>org.refcodes.component.Destroyable#destroy()</code> method.
 */
public interface Disposable {

	/**
	 * Disposes the implementing class's instance. Forces an object to be
	 * disposed. The programmer implementing this method has to take care that
	 * all objects are to be released which are referenced by the object
	 * implementing this interface and the GenericDisposeabelPublic interface.
	 * All references are to be set to a null pointer in order for the garbage
	 * collector to work more effectively. If this method has been called than
	 * the method isDisposed() in the interface GenericDisposeablePublic has to
	 * return true. If dispose() has been called, than ALL OTHER methods in the
	 * class implementing this interface and the corresponding interface
	 * GenericDisposeablePublic except the isDispoed() and the dispose() methods
	 * have to throw a DisposedPredictableException - the programmer
	 * implementing the interface has to take care of it.
	 */
	void dispose();
}
