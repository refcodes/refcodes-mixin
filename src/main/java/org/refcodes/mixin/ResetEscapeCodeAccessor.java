// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a reset Escape-Code property.
 */
public interface ResetEscapeCodeAccessor {

	/**
	 * Retrieves the reset Escape-Code from the reset Escape-Code property.
	 * 
	 * @return The reset Escape-Code stored by the reset Escape-Code property.
	 */
	String getResetEscapeCode();

	/**
	 * Provides a mutator for a reset Escape-Code property.
	 */
	public interface ResetEscapeCodeMutator {

		/**
		 * Sets the reset Escape-Code for the reset Escape-Code property.
		 * 
		 * @param aResetEscCode The reset Escape-Code to be stored by the reset
		 *        Escape-Code property.
		 */
		void setResetEscapeCode( String aResetEscCode );
	}

	/**
	 * Provides a builder method for a reset Escape-Code property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ResetEscapeCodeBuilder<B extends ResetEscapeCodeBuilder<B>> {

		/**
		 * Sets the reset Escape-Code for the reset Escape-Code property.
		 * 
		 * @param aResetEscCode The reset Escape-Code to be stored by the reset
		 *        Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withResetEscapeCode( String aResetEscCode );
	}

	/**
	 * Provides a reset Escape-Code property.
	 */
	public interface ResetEscapeCodeProperty extends ResetEscapeCodeAccessor, ResetEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setResetEscapeCode(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aResetEscCode The {@link String} to set (via
		 *        {@link #setResetEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letResetEscapeCode( String aResetEscCode ) {
			setResetEscapeCode( aResetEscCode );
			return aResetEscCode;
		}
	}
}
