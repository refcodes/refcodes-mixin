// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a column width property.
 */
public interface ColumnWidthAccessor {

	/**
	 * Retrieves the column width from the column width property.
	 * 
	 * @return The column width stored by the column width property.
	 */
	int getColumnWidth();

	/**
	 * Provides a mutator for a column width property.
	 */
	public interface ColumnWidthMutator {

		/**
		 * Sets the column width for the column width property.
		 * 
		 * @param aColumnWidth The column width to be stored by the column width
		 *        property.
		 */
		void setColumnWidth( int aColumnWidth );
	}

	/**
	 * Provides a builder method for a column width property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ColumnWidthBuilder<B extends ColumnWidthBuilder<B>> {

		/**
		 * Sets the column width for the column width property.
		 * 
		 * @param aColumnWidth The column width to be stored by the column width
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withColumnWidth( int aColumnWidth );
	}

	/**
	 * Provides a column width property.
	 */
	public interface ColumnWidthProperty extends ColumnWidthAccessor, ColumnWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setColumnWidth(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aColumnWidth The integer to set (via
		 *        {@link #setColumnWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letColumnWidth( int aColumnWidth ) {
			setColumnWidth( aColumnWidth );
			return aColumnWidth;
		}
	}
}
