// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpattern ("http://www.gnu.org/software/classpattern/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.util.regex.Pattern;

/**
 * Provides an accessor for a pattern property.
 */
public interface PatternAccessor {

	/**
	 * Retrieves the pattern from the pattern property.
	 * 
	 * @return The pattern stored by the pattern property.
	 */
	Pattern getPattern();

	/**
	 * Provides a mutator for a pattern property.
	 */
	public interface PatternMutator {

		/**
		 * Sets the pattern for the pattern property.
		 * 
		 * @param aPattern The pattern to be stored by the pattern property.
		 */
		void setPattern( Pattern aPattern );
	}

	/**
	 * Provides a mutator for an pattern property.
	 * 
	 * @param <B> The builder which implements the {@link PatternBuilder}.
	 */
	public interface PatternBuilder<B extends PatternBuilder<?>> {

		/**
		 * Sets the pattern to use and returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aPattern The pattern to be stored by the pattern property.
		 * 
		 * @return This {@link PatternBuilder} instance to continue
		 *         configuration.
		 */
		B withPattern( Pattern aPattern );
	}

	/**
	 * Provides a pattern property.
	 */
	public interface PatternProperty extends PatternAccessor, PatternMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Pattern} (setter)
		 * as of {@link #setPattern(Pattern)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPattern The {@link Pattern} to set (via
		 *        {@link #setPattern(Pattern)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Pattern letPattern( Pattern aPattern ) {
			setPattern( aPattern );
			return aPattern;
		}
	}
}
