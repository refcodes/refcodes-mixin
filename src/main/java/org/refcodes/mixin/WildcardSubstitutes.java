// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * The {@link WildcardSubstitutes} contains all information available regarding
 * substitution of wildcards in path declarations.
 */
public interface WildcardSubstitutes {

	/**
	 * Returns an array of the wildcard substitutes for the wildcards in your
	 * path pattern compared to the actual path. The order of the wildcard
	 * substitutes aligns to the order of the wildcards (from left to right)
	 * defined in your path pattern.
	 * 
	 * @return The text substituting the wildcards in the order of the wildcards
	 *         being substituted or null if there are none such substitutes.
	 */
	String[] getWildcardReplacements();

	/**
	 * Returns the wildcard substitute for the wildcards in your path pattern
	 * compared to the actual path. The text of the wildcard substitute aligns
	 * to the index of the wildcard (from left to right) as defined in your path
	 * pattern.
	 * 
	 * @param aIndex The index of the wildcard in question for which to retrieve
	 *        the substitute.
	 * 
	 * @return The text substituting the wildcard at the given path pattern's
	 *         wildcard index or null if there is none such substitute.
	 */
	String getWildcardReplacementAt( int aIndex );

	/**
	 * Returns the wildcard substitutes for the wildcards in your path pattern
	 * compared to the actual path. The text of the wildcard substitutes aligns
	 * to the index of the wildcards (from left to right) as defined in your
	 * path pattern.
	 * 
	 * @param aIndexes The index of the wildcards in question for which to
	 *        retrieve the substitutes.
	 * 
	 * @return The text substituting the wildcards at the given path pattern's
	 *         wildcard indexes or null if there is none such substitute.
	 */
	String[] getWildcardReplacementsAt( int... aIndexes );

	/**
	 * Returns the wildcard substitute for the wildcards in your path pattern
	 * compared to the actual path. The text of the wildcard substitute aligns
	 * to the name of the wildcard (as defined in your path pattern).
	 * 
	 * @param aWildcardName The name of the wildcard in question for which to
	 *        retrieve the substitute.
	 * 
	 * @return The text substituting the wildcard with the given path pattern's
	 *         wildcard name or null if there is none such substitute.
	 */
	String getWildcardReplacement( String aWildcardName );

	/**
	 * Returns the wildcard substitutes for the wildcards in your path pattern
	 * compared to the actual path. The text of the wildcard substitutes aligns
	 * to the order of the provided wildcard names (as defined in your path
	 * pattern).
	 * 
	 * @param aWildcardNames The names of the wildcards in question for which to
	 *        retrieve the substitutes in the order of the provided names.
	 * 
	 * @return The text substituting the wildcard with the given path pattern's
	 *         wildcard names or null if there are none such substitute.
	 */
	String[] getWildcardReplacements( String... aWildcardNames );

	/**
	 * Retrieves the array of wildcard names (aliases) in this instance.
	 * 
	 * @return The array of wildcard aliases or null if there are none named
	 *         wildcards.
	 */
	String[] getWildcardNames();

}
