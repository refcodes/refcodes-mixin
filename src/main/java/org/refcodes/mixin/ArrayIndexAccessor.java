// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a document array index property. Determines whether
 * marshaling (XML) documents will preserve array index information by using an
 * index attribute for array elements in the produced XML.
 */
public interface ArrayIndexAccessor {

	/**
	 * When <code>true</code> then marshaling (XML) documents will preserve
	 * array index information by using an index attribute for array elements in
	 * the produced XML.
	 * 
	 * @return When true then array index information is preserved in an
	 *         element's attribute.
	 */
	boolean isArrayIndex();

	/**
	 * Provides a mutator for a document array index property.
	 */
	public interface ArrayIndexMutator {

		/**
		 * Sets the document array index status for the document array index
		 * property.
		 * 
		 * @param isArrayIndex The document array index status to be stored by
		 *        the document array index property.
		 */
		void setArrayIndex( boolean isArrayIndex );
	}

	/**
	 * Provides a builder for a document array index property.
	 *
	 * @param <B> the generic type
	 */
	public interface ArrayIndexBuilder<B extends ArrayIndexBuilder<B>> {

		/**
		 * Builder method for the
		 * {@link ArrayIndexMutator#setArrayIndex(boolean)} method.
		 * 
		 * @param isArrayIndex The document array index status to be stored by
		 *        the document array index property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withArrayIndex( boolean isArrayIndex );
	}

	/**
	 * Provides a document array index property.
	 */
	public interface ArrayIndexProperty extends ArrayIndexAccessor, ArrayIndexMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setArrayIndex(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isArrayIndex The boolean to set (via
		 *        {@link #setArrayIndex(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letArrayIndex( boolean isArrayIndex ) {
			setArrayIndex( isArrayIndex );
			return isArrayIndex;
		}
	}
}
