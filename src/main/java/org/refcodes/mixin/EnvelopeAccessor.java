// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a document envelope property. Determines whether
 * unmarshaling (XML) documents will preserve the preserve the root element
 * (envelope). As an (XML) document requires a root element, the root element
 * often is provided merely as of syntactic reasons and must be omitted as of
 * semantic reasons.
 */
public interface EnvelopeAccessor {

	/**
	 * When <code>true</code> then unmarshaling (XML) documents will preserve
	 * the preserve the root element (envelope). As an (XML) document requires a
	 * root element, the root element often is provided merely as of syntactic
	 * reasons and must be omitted as of semantic reasons. Unmarshaling
	 * functionality therefore by default skips the root elelemt, as this is
	 * considered merely to serve as an envelope. This behavior can be
	 * overridden by setting this property to <code>true</code>.
	 * 
	 * @return True when preserving an (XML) document's root element (envelope).
	 */
	boolean isEnvelope();

	/**
	 * Provides a mutator for a document envelope property.
	 */
	public interface EnvelopeMutator {

		/**
		 * Sets the document envelope status for the document envelope property.
		 * 
		 * @param isEnvelope The document envelope status to be stored by the
		 *        document envelope property.
		 */
		void setEnvelope( boolean isEnvelope );
	}

	/**
	 * Provides a builder for a document envelope property.
	 *
	 * @param <B> the generic type
	 */
	public interface EnvelopeBuilder<B extends EnvelopeBuilder<B>> {

		/**
		 * Builder method for the {@link EnvelopeMutator#setEnvelope(boolean)}
		 * method.
		 * 
		 * @param isEnvelope The document envelope status to be stored by the
		 *        document envelope property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withEnvelope( boolean isEnvelope );
	}

	/**
	 * Provides a document envelope property.
	 */
	public interface EnvelopeProperty extends EnvelopeAccessor, EnvelopeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setEnvelope(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isEnvelope The boolean to set (via
		 *        {@link #setEnvelope(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letEnvelope( boolean isEnvelope ) {
			setEnvelope( isEnvelope );
			return isEnvelope;
		}
	}
}
