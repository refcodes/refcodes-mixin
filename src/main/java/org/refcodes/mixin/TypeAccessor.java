// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a type property.
 * 
 * @param <T> The type of the type of the generic type (lol)
 */
public interface TypeAccessor<T> {

	/**
	 * Retrieves the type property.
	 * 
	 * @return The type stored by the property.
	 */
	Class<T> getType();

	/**
	 * Provides a mutator for a type property.
	 * 
	 * @param <T> The type of the type of the generic type (lol)
	 */
	public interface TypeMutator<T> {

		/**
		 * Sets the type for the property.
		 * 
		 * @param aType The type to be stored by the property.
		 */
		void setType( Class<T> aType );
	}

	/**
	 * Provides a builder method for a type property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <T> The type of the type.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TypeBuilder<T, B extends TypeBuilder<T, B>> {

		/**
		 * Sets the type for the type property.
		 * 
		 * @param aType The type to be stored by the type property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withType( Class<T> aType );
	}

	/**
	 * Provides a type property.
	 * 
	 * @param <T> The type of the type of the generic type (lol)
	 */
	public interface TypeProperty<T> extends TypeAccessor<T>, TypeMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setType(Class)} and returns the very same value (getter).
		 * 
		 * @param aType The value to set (via {@link #setType(Class)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Class<T> letType( Class<T> aType ) {
			setType( aType );
			return aType;
		}
	}
}
