// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a title property.
 */
public interface TitleAccessor {

	/**
	 * Retrieves the title from the title property.
	 * 
	 * @return The title stored by the title property.
	 */
	String getTitle();

	/**
	 * Provides a mutator for a title property.
	 */
	public interface TitleMutator {

		/**
		 * Sets the title for the title property.
		 * 
		 * @param aTitle The title to be stored by the title property.
		 */
		void setTitle( String aTitle );
	}

	/**
	 * Provides a builder method for a title property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TitleBuilder<B extends TitleBuilder<B>> {

		/**
		 * Sets the title for the title property.
		 * 
		 * @param aTitle The title to be stored by the title property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTitle( String aTitle );
	}

	/**
	 * Provides a title property.
	 */
	public interface TitleProperty extends TitleAccessor, TitleMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setTitle(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aTitle The {@link String} to set (via
		 *        {@link #setTitle(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letTitle( String aTitle ) {
			setTitle( aTitle );
			return aTitle;
		}
	}
}
