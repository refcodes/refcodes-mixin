// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a valid property.
 */
public interface ValidAccessor {

	/**
	 * Retrieves the valid status from the valid property.
	 * 
	 * @return The valid status stored by the valid property.
	 */
	boolean isValid();

	/**
	 * Provides a mutator for a valid property.
	 */
	public interface ValidMutator {

		/**
		 * Sets the valid status for the valid property.
		 * 
		 * @param isValid The valid status to be stored by the valid property.
		 */
		void setValid( boolean isValid );
	}

	/**
	 * Provides a builder for a valid property.
	 *
	 * @param <B> the generic type
	 */
	public interface ValidBuilder<B extends ValidBuilder<B>> extends ValidMutator {

		/**
		 * Builder method for the {@link #setValid(boolean)} method.
		 * 
		 * @param isValid The valid status to be stored by the valid property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withValid( boolean isValid );
	}

	/**
	 * Provides a valid property.
	 */
	public interface ValidProperty extends ValidAccessor, ValidMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setValid(boolean)} and returns the very same value (getter).
		 * 
		 * @param isValid The boolean to set (via {@link #setValid(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letValid( boolean isValid ) {
			setValid( isValid );
			return isValid;
		}
	}
}
