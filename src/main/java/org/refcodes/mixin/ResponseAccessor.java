// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a response property for e.g. key / response pair.
 *
 * @param <RES> The type of the response to be used.
 */
public interface ResponseAccessor<RES> {

	/**
	 * Retrieves the response from the response property.
	 * 
	 * @return The response stored by the response property.
	 */
	RES getResponse();

	/**
	 * Extends the {@link ResponseAccessor} with a setter method.
	 * 
	 * @param <RES> The type of the response property.
	 */
	public interface ResponseMutator<RES> {

		/**
		 * Sets the response for the response property.
		 * 
		 * @param aResponse The response to be stored by the response property.
		 */
		void setResponse( RES aResponse );

	}

	/**
	 * Provides a builder method for a response property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <RES> The type of the response to be used.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ResponseBuilder<RES, B extends ResponseBuilder<RES, B>> {

		/**
		 * Sets the response for the response property.
		 * 
		 * @param aResponse The response to be stored by the response property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withResponse( RES aResponse );
	}

	/**
	 * Extends the {@link ResponseAccessor} with a setter method.
	 * 
	 * @param <RES> The type of the response property.
	 */
	public interface ResponseProperty<RES> extends ResponseAccessor<RES>, ResponseMutator<RES> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setResponse(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aResponse The value to set (via {@link #setResponse(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default RES letResponse( RES aResponse ) {
			setResponse( aResponse );
			return aResponse;
		}
	}
}
