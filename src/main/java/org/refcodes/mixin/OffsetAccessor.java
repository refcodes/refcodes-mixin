// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a offset property.
 */
public interface OffsetAccessor {

	/**
	 * Retrieves the offset from the offset property.
	 * 
	 * @return The offset stored by the offset property.
	 */
	int getOffset();

	/**
	 * Provides a mutator for a offset property.
	 */
	public interface OffsetMutator {

		/**
		 * Sets the offset for the offset property.
		 * 
		 * @param aOffset The offset to be stored by the offset property.
		 */
		void setOffset( int aOffset );
	}

	/**
	 * Provides a builder method for a offset property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface OffsetBuilder<B extends OffsetBuilder<B>> {

		/**
		 * Sets the offset for the offset property.
		 * 
		 * @param aOffset The offset to be stored by the offset property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withOffset( int aOffset );
	}

	/**
	 * Provides a offset property.
	 */
	public interface OffsetProperty extends OffsetAccessor, OffsetMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setOffset(int)} and returns the very same value (getter).
		 * 
		 * @param aOffset The integer to set (via {@link #setOffset(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letOffset( int aOffset ) {
			setOffset( aOffset );
			return aOffset;
		}
	}
}
