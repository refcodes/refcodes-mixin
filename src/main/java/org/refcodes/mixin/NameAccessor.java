// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a name property.
 */
public interface NameAccessor {

	/**
	 * Retrieves the name from the name property.
	 * 
	 * @return The name stored by the name property.
	 */
	String getName();

	/**
	 * Provides a mutator for a name property.
	 */
	public interface NameMutator {

		/**
		 * Sets the name for the name property.
		 * 
		 * @param aName The name to be stored by the name property.
		 */
		void setName( String aName );
	}

	/**
	 * Provides a builder method for a name property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface NameBuilder<B extends NameBuilder<B>> {

		/**
		 * Sets the name for the name property.
		 * 
		 * @param aName The name to be stored by the name property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withName( String aName );
	}

	/**
	 * Provides a name property.
	 */
	public interface NameProperty extends NameAccessor, NameMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setName(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aName The {@link String} to set (via
		 *        {@link #setName(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letName( String aName ) {
			setName( aName );
			return aName;
		}
	}
}
