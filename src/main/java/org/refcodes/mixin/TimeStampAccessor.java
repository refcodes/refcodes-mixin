// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a timeStamp property.
 */
public interface TimeStampAccessor {

	/**
	 * Retrieves the time stamp from the time stamp property.
	 * 
	 * @return The timeStamp stored by the time stamp property.
	 */
	double getTimeStamp();

	/**
	 * Provides a mutator for a timeStamp property.
	 */
	public interface TimeStampMutator {

		/**
		 * Sets the time stamp for the time stamp property.
		 * 
		 * @param aTimeMillis The timeStamp to be stored by the time stamp
		 *        property.
		 */
		void setTimeStamp( double aTimeMillis );
	}

	/**
	 * Provides a builder method for a timeStamp property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TimeStampBuilder<B extends TimeStampBuilder<B>> {

		/**
		 * Sets the time stamp for the time stamp property.
		 * 
		 * @param aTimeMillis The timeStamp to be stored by the time stamp
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTimeStamp( double aTimeMillis );
	}

	/**
	 * Provides a timeStamp property.
	 */
	public interface TimeStampProperty extends TimeStampAccessor, TimeStampMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given double (setter) as of
		 * {@link #setTimeStamp(double)} and returns the very same value
		 * (getter).
		 * 
		 * @param aTimeMillis The double to set (via
		 *        {@link #setTimeStamp(double)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default double letTimeStamp( double aTimeMillis ) {
			setTimeStamp( aTimeMillis );
			return aTimeMillis;
		}
	}
}
