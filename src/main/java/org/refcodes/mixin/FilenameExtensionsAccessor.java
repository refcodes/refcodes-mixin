// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a filenames extension property. Each filename
 * extensions does not(!) contain the dot "." separating the extension from the
 * name, e.g. "ini" (instead of ".ini").
 */
public interface FilenameExtensionsAccessor {

	/**
	 * Retrieves the filenames extension from the filenames extension property.
	 * Each filename extensions does not(!) contain the dot "." separating the
	 * extension from the name, e.g. "ini" (instead of ".ini").
	 * 
	 * @return The filenames extension stored by the filenames extension
	 *         property.
	 */
	String[] getFilenameExtensions();

	/**
	 * Provides a mutator for a filenames extension property.
	 */
	public interface FilenameExtensionsMutator {

		/**
		 * Sets the filenames extension for the filenames extension property.
		 * Each filename extensions does not(!) contain the dot "." separating
		 * the extension from the name, e.g. "ini" (instead of ".ini").
		 * 
		 * @param aFilenameExtensions The filenames extension to be stored by
		 *        the filenames extension property.
		 */
		void setFilenameExtensions( String[] aFilenameExtensions );
	}

	/**
	 * Provides a builder method for a filenames extension property returning
	 * the builder for applying multiple build operations. Each filename
	 * extensions does not(!) contain the dot "." separating the extension from
	 * the name, e.g. "ini" (instead of ".ini").
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FilenameExtensionsBuilder<B extends FilenameExtensionsBuilder<B>> {

		/**
		 * Sets the filenames extension for the filenames extension property.
		 * Each filename extensions does not(!) contain the dot "." separating
		 * the extension from the name, e.g. "ini" (instead of ".ini").
		 * 
		 * @param aFilenameExtensions The filenames extension to be stored by
		 *        the filenames extension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFilenameExtensions( String[] aFilenameExtensions );
	}

	/**
	 * Provides a filenames extension property. Each filename extensions does
	 * not(!) contain the dot "." separating the extension from the name, e.g.
	 * "ini" (instead of ".ini").
	 */
	public interface FilenameExtensionsProperty extends FilenameExtensionsAccessor, FilenameExtensionsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} array
		 * (setter) as of {@link #setFilenameExtensions(String[])} and returns
		 * the very same value (getter).
		 * 
		 * @param aFilenameExtensions The {@link String} array to set (via
		 *        {@link #setFilenameExtensions(String[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String[] letFilenameExtensions( String[] aFilenameExtensions ) {
			setFilenameExtensions( aFilenameExtensions );
			return aFilenameExtensions;
		}
	}
}
