// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a family property.
 * 
 * @param <T> The type of the family.
 */
public interface FamilyAccessor<T> {

	/**
	 * Retrieves the family from the family property.
	 * 
	 * @return The family stored by the family property.
	 */
	T getFamily();

	/**
	 * Provides a mutator for a family property.
	 * 
	 * @param <T> The type of the family.
	 */
	public interface FamilyMutator<T> {

		/**
		 * Sets the family for the family property.
		 * 
		 * @param aFamily The family to be stored by the family property.
		 */
		void setFamily( T aFamily );
	}

	/**
	 * Provides a builder method for a family property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <T> The type of the family.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FamilyBuilder<T, B extends FamilyBuilder<T, B>> {

		/**
		 * Sets the family for the family property.
		 * 
		 * @param aFamily The family to be stored by the family property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFamily( T aFamily );
	}

	/**
	 * Provides a family property.
	 * 
	 * @param <T> The type of the family.
	 */
	public interface FamilyProperty<T> extends FamilyAccessor<T>, FamilyMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setFamily(Object)} and returns the very same value (getter).
		 * 
		 * @param aFamily The value to set (via {@link #setFamily(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letFamily( T aFamily ) {
			setFamily( aFamily );
			return aFamily;
		}
	}
}
