// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a object property.
 */
public interface ObjectAccessor {

	/**
	 * Retrieves the object from the object property.
	 * 
	 * @return The object stored by the object property.
	 */
	Object getObject();

	/**
	 * Provides a mutator for a object property.
	 */
	public interface ObjectMutator {

		/**
		 * Sets the object for the object property.
		 * 
		 * @param aObject The object to be stored by the object property.
		 */
		void setObject( Object aObject );
	}

	/**
	 * Provides a builder method for a object property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ObjectBuilder<B extends ObjectBuilder<B>> {

		/**
		 * Sets the object for the object property.
		 * 
		 * @param aObject The object to be stored by the object property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withObject( Object aObject );
	}

	/**
	 * Provides a object property.
	 */
	public interface ObjectProperty extends ObjectAccessor, ObjectMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setObject(Object)} and returns the very same value (getter).
		 * 
		 * @param aObject The value to set (via {@link #setObject(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Object letObject( Object aObject ) {
			setObject( aObject );
			return aObject;
		}
	}
}
