// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a body property.
 *
 * @param <T> The type of the body to be used.
 */
public interface BodyAccessor<T> {

	/**
	 * Retrieves the value from the body property.
	 * 
	 * @return The body stored by the body property.
	 */
	T getBody();

	/**
	 * Provides a mutator for a body property.
	 * 
	 * @param <T> The type of the body property.
	 */
	public interface BodyMutator<T> {

		/**
		 * Sets the body for the body property.
		 * 
		 * @param aBody The body to be stored by the body property.
		 */
		void setBody( T aBody );
	}

	/**
	 * Provides a builder method for a body property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <T> the body type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BodyBuilder<T, B extends BodyBuilder<T, B>> {

		/**
		 * Sets the body for the body property.
		 * 
		 * @param aBody The body to be stored by the body property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBody( T aBody );
	}

	/**
	 * Provides a body property.
	 * 
	 * @param <T> The type of the body property.
	 */
	public interface BodyProperty<T> extends BodyAccessor<T>, BodyMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setBody(Object)} and returns the very same value (getter).
		 * 
		 * @param aBody The value to set (via {@link #setBody(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letBody( T aBody ) {
			setBody( aBody );
			return aBody;
		}
	}
}
