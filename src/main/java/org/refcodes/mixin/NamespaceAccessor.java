// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a namespace property.
 */
public interface NamespaceAccessor {

	/**
	 * Retrieves the namespace from the namespace property.
	 * 
	 * @return The namespace stored by the namespace property.
	 */
	String getNamespace();

	/**
	 * Provides a mutator for a namespace property.
	 */
	public interface NamespaceMutator {

		/**
		 * Sets the namespace for the namespace property.
		 * 
		 * @param aNamespace The namespace to be stored by the namespace
		 *        property.
		 */
		void setNamespace( String aNamespace );
	}

	/**
	 * Provides a builder method for a namespace property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface NamespaceBuilder<B extends NamespaceBuilder<B>> {

		/**
		 * Sets the namespace for the namespace property.
		 * 
		 * @param aNamespace The namespace to be stored by the namespace
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withNamespace( String aNamespace );
	}

	/**
	 * Provides a namespace property.
	 */
	public interface NamespaceProperty extends NamespaceAccessor, NamespaceMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setNamespace(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aNamespace The {@link String} to set (via
		 *        {@link #setNamespace(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letNamespace( String aNamespace ) {
			setNamespace( aNamespace );
			return aNamespace;
		}
	}
}
