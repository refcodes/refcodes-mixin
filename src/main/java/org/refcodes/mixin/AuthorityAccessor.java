// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a credential property for e.g. key / credential
 * pair.
 *
 * @param <AUTH> The type of the credential/Authority to be used.
 */
public interface AuthorityAccessor<AUTH> {

	/**
	 * Retrieves the credential from the credential property.
	 * 
	 * @return The credential stored by the credential property.
	 */
	AUTH getAuthority();

	/**
	 * Provides a mutator for a credential property for e.g. key / credential
	 * pair.
	 * 
	 * @param <AUTH> The type of the credential property.
	 */
	public interface AuthorityMutator<AUTH> {

		/**
		 * Sets the credential for the credential property.
		 * 
		 * @param aAuthority The credential/authority to be stored by the
		 *        credential property.
		 */
		void setAuthority( AUTH aAuthority );

	}

	/**
	 * * Provides a credential property for e.g. key / credential pair.
	 * 
	 * @param <AUTH> The type of the credential property.
	 */
	public interface AuthorityProperty<AUTH> extends AuthorityAccessor<AUTH>, AuthorityMutator<AUTH> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setAuthority(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aAuthority The value to set (via
		 *        {@link #setAuthority(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default AUTH letAuthority( AUTH aAuthority ) {
			setAuthority( aAuthority );
			return aAuthority;
		}
	}
}
