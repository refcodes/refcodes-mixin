// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.io.PrintStream;

/**
 * Provides an accessor for an print stream property.
 */
public interface PrintStreamAccessor {

	/**
	 * Retrieves the print stream from the print stream property.
	 * 
	 * @return The print stream stored by the print stream property.
	 */
	PrintStream getPrintStream();

	/**
	 * Provides a mutator for an print stream property.
	 */
	public interface PrintStreamMutator {

		/**
		 * Sets the print stream for the print stream property.
		 * 
		 * @param aPrintStream The print stream to be stored by the print stream
		 *        property.
		 */
		void setPrintStream( PrintStream aPrintStream );
	}

	/**
	 * Provides a mutator for an print stream property.
	 * 
	 * @param <B> The builder which implements the {@link PrintStreamBuilder}.
	 */
	public interface PrintStreamBuilder<B extends PrintStreamBuilder<?>> {

		/**
		 * Sets the print stream to use and returns this builder as of the
		 * Builder-Pattern.
		 * 
		 * @param aPrintStream The print stream to be stored by the print stream
		 *        property.
		 * 
		 * @return This {@link PrintStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withPrintStream( PrintStream aPrintStream );
	}

	/**
	 * Provides an print stream property.
	 */
	public interface PrintStreamProperty extends PrintStreamAccessor, PrintStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link PrintStream}
		 * (setter) as of {@link #setPrintStream(PrintStream)} and returns the
		 * very same value (getter).
		 * 
		 * @param aPrintStream The {@link PrintStream} to set (via
		 *        {@link #setPrintStream(PrintStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default PrintStream letPrintStream( PrintStream aPrintStream ) {
			setPrintStream( aPrintStream );
			return aPrintStream;
		}
	}
}
