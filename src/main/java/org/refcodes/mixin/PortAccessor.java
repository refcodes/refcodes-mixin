// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a port property.
 */
public interface PortAccessor {

	/**
	 * Retrieves the port from the port property.
	 * 
	 * @return The port stored by the port property.
	 */
	int getPort();

	/**
	 * Provides a mutator for a port property.
	 */
	public interface PortMutator {

		/**
		 * Sets the port for the port property.
		 * 
		 * @param aPort The port to be stored by the port property.
		 */
		void setPort( int aPort );
	}

	/**
	 * Provides a builder method for a port property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PortBuilder<B extends PortBuilder<B>> {

		/**
		 * Sets the port for the port property.
		 * 
		 * @param aPort The port to be stored by the port property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPort( int aPort );
	}

	/**
	 * Provides a port property.
	 */
	public interface PortProperty extends PortAccessor, PortMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setPort(int)} and returns the very same value (getter).
		 * 
		 * @param aPort The integer to set (via {@link #setPort(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letPort( int aPort ) {
			setPort( aPort );
			return aPort;
		}
	}
}
