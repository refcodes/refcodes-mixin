// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a encoding property.
 * 
 * @param <E> The type of the encoding.
 */
public interface EncodingAccessor<E> {

	/**
	 * Retrieves the encoding from the encoding property.
	 * 
	 * @return The encoding stored by the encoding property.
	 */
	E getEncoding();

	/**
	 * Provides a mutator for a encoding property.
	 * 
	 * @param <E> The type of the encoding.
	 */
	public interface EncodingMutator<E> {

		/**
		 * Sets the encoding for the encoding property.
		 * 
		 * @param aEncoding The encoding to be stored by the encoding property.
		 */
		void setEncoding( E aEncoding );
	}

	/**
	 * Provides a builder method for a encoding property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <E> The type of the encoding.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface EncodingBuilder<E, B extends EncodingBuilder<E, B>> {

		/**
		 * Sets the encoding for the encoding property.
		 * 
		 * @param aEncoding The encoding to be stored by the encoding property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withEncoding( E aEncoding );
	}

	/**
	 * Provides a encoding property.
	 * 
	 * @param <E> The type of the encoding.
	 */
	public interface EncodingProperty<E> extends EncodingAccessor<E>, EncodingMutator<E> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setEncoding(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aEncoding The value to set (via {@link #setEncoding(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default E letEncoding( E aEncoding ) {
			setEncoding( aEncoding );
			return aEncoding;
		}
	}
}
