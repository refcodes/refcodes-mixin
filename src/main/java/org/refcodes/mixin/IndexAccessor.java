// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a index property.
 */
public interface IndexAccessor {

	/**
	 * Retrieves the index from the index property.
	 * 
	 * @return The index stored by the index property.
	 */
	long getIndex();

	/**
	 * Provides a mutator for a index property.
	 */
	public interface IndexMutator {

		/**
		 * Sets the index for the index property.
		 * 
		 * @param aIndex The index to be stored by the index property.
		 */
		void setIndex( long aIndex );

		/**
		 * Increases the index.
		 */
		void increaseIndex();

		/**
		 * Decreases the index.
		 */
		void decreaseIndex();

		/**
		 * Resets the index to 0;.
		 */
		default void resetIndex() {
			setIndex( 0 );
		}
	}

	/**
	 * Provides a builder method for a index property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface IndexBuilder<B extends IndexBuilder<B>> {

		/**
		 * Sets the index for the index property.
		 * 
		 * @param aIndex The index to be stored by the index property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withIndex( long aIndex );

		/**
		 * Increases the index.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withIncreaseIndex();

		/**
		 * Decreases the index.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDecreaseIndex();
	}

	/**
	 * Provides a index property.
	 */
	public interface IndexProperty extends IndexAccessor, IndexMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setIndex(long)} and returns the very same value (getter).
		 * 
		 * @param aIndex The long to set (via {@link #setIndex(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letIndex( long aIndex ) {
			setIndex( aIndex );
			return aIndex;
		}
	}
}
