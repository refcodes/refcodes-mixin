// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.util.Date;

/**
 * Provides an accessor for a "created" date property.
 */
public interface CreatedDateAccessor {

	/**
	 * Retrieves the "created" date from the date property.
	 * 
	 * @return The "created" date stored by the date property.
	 */
	Date getCreatedDate();

	/**
	 * Provides a mutator for a "created" date property.
	 */
	public interface CreatedDateMutator {

		/**
		 * Sets the "created" date for the date property.
		 * 
		 * @param aCreatedDate The "created" date to be stored by the date
		 *        property.
		 */
		void setCreatedDate( Date aCreatedDate );
	}

	/**
	 * Extends the {@link CreatedDateAccessor} with a setter method.
	 */
	public interface CreatedDateProperty extends CreatedDateAccessor, CreatedDateMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Date} (setter) as
		 * of {@link #setCreatedDate(Date)} and returns the very same value
		 * (getter).
		 * 
		 * @param aCreatedDate The {@link Date} to set (via
		 *        {@link #setCreatedDate(Date)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Date letCreatedDate( Date aCreatedDate ) {
			setCreatedDate( aCreatedDate );
			return aCreatedDate;
		}
	}
}
