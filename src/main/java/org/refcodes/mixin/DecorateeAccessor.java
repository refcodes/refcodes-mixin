// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a decoratee property (as of the decorator pattern).
 *
 * @param <DECORATEE> The type of the decoratee to be used.
 */
public interface DecorateeAccessor<DECORATEE> {

	/**
	 * Retrieves the value from the decoratee property.
	 * 
	 * @return The decoratee stored by the decoratee property.
	 */
	DECORATEE getDecoratee();

	/**
	 * Provides a mutator for a decoratee property.
	 * 
	 * @param <DECORATEE> The type of the decoratee property.
	 */
	public interface DecorateeMutator<DECORATEE> {

		/**
		 * Sets the decoratee for the decoratee property.
		 * 
		 * @param aDecoratee The decoratee to be stored by the decoratee
		 *        property.
		 */
		void setDecoratee( DECORATEE aDecoratee );
	}

	/**
	 * Provides a builder method for a decoratee property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <DECORATEE> the decoratee type.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DecorateeBuilder<DECORATEE, B extends DecorateeBuilder<DECORATEE, B>> {

		/**
		 * Sets the decoratee for the decoratee property.
		 * 
		 * @param aDecoratee The decoratee to be stored by the decoratee
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDecoratee( DECORATEE aDecoratee );
	}

	/**
	 * Provides a decoratee property.
	 * 
	 * @param <DECORATEE> The type of the decoratee property.
	 */
	public interface DecorateeProperty<DECORATEE> extends DecorateeAccessor<DECORATEE>, DecorateeMutator<DECORATEE> {

		/**
		 * Sets the given decoratee (setter) as of {@link #setDecoratee(Object)}
		 * and returns the very same decoratee (getter). This method stores and
		 * passes through the given decoratee, which is very useful for builder
		 * APIs.
		 * 
		 * @param aDecoratee The decoratee to set (via
		 *        {@link #setDecoratee(Object)}).
		 * 
		 * @return Returns decoratee passed for it to be used in conclusive
		 *         processing steps.
		 */
		default DECORATEE letDecoratee( DECORATEE aDecoratee ) {
			setDecoratee( aDecoratee );
			return aDecoratee;
		}
	}
}
