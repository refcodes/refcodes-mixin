// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for an encoded property.
 */
public interface EncodedAccessor {

	/**
	 * Retrieves the encoded representation from the encoded property.
	 * 
	 * @return The encoded stored by the encoded property.
	 */
	byte[] getEncoded();

	/**
	 * Provides a mutator for an encoded property.
	 */
	public interface EncodedMutator {

		/**
		 * Sets the encoded representation for the encoded property.
		 * 
		 * @param aEncoded The encoded to be stored by the encoded property.
		 */
		void setEncoded( byte[] aEncoded );
	}

	/**
	 * Provides a builder method for an encoded property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface EncodedBuilder<B extends EncodedBuilder<B>> {

		/**
		 * Sets the encoded representation for the encoded property.
		 * 
		 * @param aEncoded The encoded to be stored by the encoded property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withEncoded( byte[] aEncoded );
	}

	/**
	 * Provides an encoded property.
	 */
	public interface EncodedProperty extends EncodedAccessor, EncodedMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given bytes (setter) as of
		 * {@link #setEncoded(byte[])} and returns the very same value (getter).
		 * 
		 * @param aEncoded The bytes to set (via {@link #setEncoded(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letEncoded( byte[] aEncoded ) {
			setEncoded( aEncoded );
			return aEncoded;
		}
	}
}
