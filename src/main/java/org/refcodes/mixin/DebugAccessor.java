// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a debug property.
 */
public interface DebugAccessor {

	/**
	 * Retrieves the debug status from the debug property.
	 * 
	 * @return The debug status stored by the debug property.
	 */
	boolean isDebug();

	/**
	 * Provides a mutator for a debug property.
	 */
	public interface DebugMutator {

		/**
		 * Sets the debug status for the debug property.
		 * 
		 * @param isDebug The debug status to be stored by the debug property.
		 */
		void setDebug( boolean isDebug );
	}

	/**
	 * Provides a debug property.
	 */
	public interface DebugProperty extends DebugAccessor, DebugMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setDebug(boolean)} and returns the very same value (getter).
		 * 
		 * @param isDebug The boolean to set (via {@link #setDebug(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letDebug( boolean isDebug ) {
			setDebug( isDebug );
			return isDebug;
		}
	}
}
