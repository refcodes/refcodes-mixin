// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.io.OutputStream;

/**
 * Provides an accessor for an output stream property.
 */
public interface OutputStreamAccessor {

	/**
	 * Retrieves the output stream from the output stream property.
	 * 
	 * @return The output stream stored by the output stream property.
	 */
	OutputStream getOutputStream();

	/**
	 * Provides a mutator for an output stream property.
	 */
	public interface OutputStreamMutator {

		/**
		 * Sets the output stream for the output stream property.
		 * 
		 * @param aOutputStream The output stream to be stored by the output
		 *        stream property.
		 */
		void setOutputStream( OutputStream aOutputStream );
	}

	/**
	 * Provides a mutator for an output stream property.
	 * 
	 * @param <B> The builder which implements the {@link OutputStreamBuilder}.
	 */
	public interface OutputStreamBuilder<B extends OutputStreamBuilder<?>> {

		/**
		 * Sets the output stream to use and returns this builder as of the
		 * Builder-Pattern.
		 * 
		 * @param aOutputStream The output stream to be stored by the output
		 *        stream property.
		 * 
		 * @return This {@link OutputStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withOutputStream( OutputStream aOutputStream );
	}

	/**
	 * Provides an output stream property.
	 */
	public interface OutputStreamProperty extends OutputStreamAccessor, OutputStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link OutputStream}
		 * (setter) as of {@link #setOutputStream(OutputStream)} and returns the
		 * very same value (getter).
		 * 
		 * @param aOutputStream The {@link OutputStream} to set (via
		 *        {@link #setOutputStream(OutputStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default OutputStream letOutputStream( OutputStream aOutputStream ) {
			setOutputStream( aOutputStream );
			return aOutputStream;
		}
	}
}
