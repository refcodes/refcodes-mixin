// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a locator property.
 * 
 * @param <T> The type of the locator
 */
public interface LocatorAccessor<T> {

	/**
	 * Retrieves the locator from the locator property.
	 * 
	 * @return The locator stored by the locator property.
	 */
	T getLocator();

	/**
	 * Provides a mutator for a locator property.
	 * 
	 * @param <T> The type of the locator
	 */
	public interface LocatorMutator<T> {

		/**
		 * Sets the locator for the locator property.
		 * 
		 * @param aLocator The locator to be stored by the resource locator
		 *        property.
		 */
		void setLocator( T aLocator );
	}

	/**
	 * Provides a builder method for a locator property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <T> The type of the locator
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LocatorBuilder<T, B extends LocatorBuilder<T, B>> {

		/**
		 * Sets the locator for the locator property.
		 * 
		 * @param aLocator The locator to be stored by the resource locator
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLocator( T aLocator );
	}

	/**
	 * Provides a locator property.
	 * 
	 * @param <T> The type of the locator
	 */
	public interface LocatorProperty<T> extends LocatorAccessor<T>, LocatorMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setLocator(Object)} and returns the very same value (getter).
		 * 
		 * @param aLocator The value to set (via {@link #setLocator(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letLocator( T aLocator ) {
			setLocator( aLocator );
			return aLocator;
		}
	}
}
