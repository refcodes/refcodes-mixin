// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a busy property. Busy semantically means that a
 * system doing things which should not be aborted, for example there are open
 * sessions currently being used, open connections currently transferring data,
 * requests currently waiting for replies and similar.
 */
public interface BusyAccessor {

	/**
	 * Retrieves the busy status from the busy property.
	 * 
	 * @return The busy status stored by the busy property.
	 */
	boolean isBusy();

	/**
	 * Provides a mutator for a busy property. Busy semantically means that a
	 * system doing things which should not be aborted, for example there are
	 * open sessions currently being used, open connections currently
	 * transferring data, requests currently waiting for replies and similar.
	 */
	public interface BusyMutator {

		/**
		 * Sets the busy status for the busy property.
		 * 
		 * @param isBusy The busy status to be stored by the busy property.
		 */
		void setBusy( boolean isBusy );

		/**
		 * Sets the busy status to true.
		 */
		default void busy() {
			setBusy( true );
		}

		/**
		 * Sets the busy status to false.
		 */
		default void unbusy() {
			setBusy( false );
		}
	}

	/**
	 * Provides a busy property. Busy semantically means that a system doing
	 * things which should not be aborted, for example there are open sessions
	 * currently being used, open connections currently transferring data,
	 * requests currently waiting for replies and similar.
	 */
	public interface BusyProperty extends BusyAccessor, BusyMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setBusy(boolean)} and returns the very same value (getter).
		 * 
		 * @param isBusy The boolean to set (via {@link #setBusy(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letBusy( boolean isBusy ) {
			setBusy( isBusy );
			return isBusy;
		}
	}
}
