// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a style property.
 * 
 * @param <T> The type of the style.
 */
public interface StyleAccessor<T> {

	/**
	 * Retrieves the style from the style property.
	 * 
	 * @return The style stored by the style property.
	 */
	T getStyle();

	/**
	 * Provides a mutator for a style property.
	 * 
	 * @param <T> The type of the style.
	 */
	public interface StyleMutator<T> {

		/**
		 * Sets the style for the style property.
		 * 
		 * @param aStyle The style to be stored by the style property.
		 */
		void setStyle( T aStyle );
	}

	/**
	 * Provides a builder method for a style property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <T> The type of the style.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface StyleBuilder<T, B extends StyleBuilder<T, B>> {

		/**
		 * Sets the style for the style property.
		 * 
		 * @param aStyle The style to be stored by the style property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withStyle( T aStyle );
	}

	/**
	 * Provides a style property.
	 * 
	 * @param <T> The type of the style.
	 */
	public interface StyleProperty<T> extends StyleAccessor<T>, StyleMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value(setter) as of
		 * {@link #setStyle(Object)} and returns the very same value (getter).
		 * 
		 * @param aStyle The valueto set (via {@link #setStyle(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letStyle( T aStyle ) {
			setStyle( aStyle );
			return aStyle;
		}
	}
}
