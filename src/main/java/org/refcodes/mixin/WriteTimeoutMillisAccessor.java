// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a write timeout in milliseconds property.
 */
public interface WriteTimeoutMillisAccessor {

	/**
	 * The timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	long getWriteTimeoutMillis();

	/**
	 * Provides a mutator for a write timeout in milliseconds property.
	 */
	public interface WriteTimeoutMillisMutator {

		/**
		 * The timeout attribute in milliseconds.
		 * 
		 * @param aWriteTimeoutMillis An long integer with the timeout in
		 *        milliseconds.
		 */
		void setWriteTimeoutMillis( long aWriteTimeoutMillis );
	}

	/**
	 * Provides a builder method for a the property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface WriteTimeoutMillisBuilder<B extends WriteTimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the the property.
		 * 
		 * @param aWriteTimeoutMillis The write timeout in milliseconds to be
		 *        stored by the write timeout property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withWriteTimeoutMillis( long aWriteTimeoutMillis );
	}

	/**
	 * Provides a write timeout in milliseconds property.
	 */
	public interface WriteTimeoutMillisProperty extends WriteTimeoutMillisAccessor, WriteTimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setWriteTimeoutMillis(long)} and returns the very same value
		 * (getter).
		 * 
		 * @param aWriteTimeoutMillis The long to set (via
		 *        {@link #setWriteTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letWriteTimeoutMillis( long aWriteTimeoutMillis ) {
			setWriteTimeoutMillis( aWriteTimeoutMillis );
			return aWriteTimeoutMillis;
		}
	}
}
