// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a suspend property. If the
 * {@link Suspendable#suspend()} method of the interface {@link Suspendable} has
 * not been called, then false is returned, else true is returned. The return
 * state has to be implemented by the programmer using this interface.
 */
public interface SuspendedAccessor {

	/**
	 * Returns the suspended status for the suspended property.
	 * 
	 * @return The suspended status to be stored by the suspended property.
	 */
	boolean isSuspended();

	/**
	 * Provides a mutator for a suspend property.
	 */
	public interface SuspendedMutator {

		/**
		 * Sets the suspended status for the suspended property.
		 * 
		 * @param isSuspended The suspended status to be stored by the suspended
		 *        property.
		 */
		void setSuspended( boolean isSuspended );
	}

	/**
	 * Provides a mutator for a suspend property.
	 */
	public interface SuspendedProperty extends SuspendedAccessor, SuspendedMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setSuspended(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isSuspended The boolean to set (via
		 *        {@link #setSuspended(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letSuspended( boolean isSuspended ) {
			setSuspended( isSuspended );
			return isSuspended;
		}
	}
}
