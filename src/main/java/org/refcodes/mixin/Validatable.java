// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * The Interface Validatable.
 *
 * @param <T> the generic type
 */
public interface Validatable<T> {

	/**
	 * Validates the provided credentials against this credentials's bearer and
	 * secret. If this credentials instance's bearer or secret are null, then
	 * false is returned.
	 * 
	 * @param aCredentials The credentials to be tested if them fit with the
	 *        this credentials instance.
	 * 
	 * @return True if the credentials match with the this credentials instance
	 *         and this credentials instance's bearer and secret are not null.
	 */
	boolean isValid( T aCredentials );

}