// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a identity property.
 */
public interface IdentityAccessor {

	/**
	 * Retrieves the identity from the identity property.
	 * 
	 * @return The identity stored by the identity property.
	 */
	String getIdentity();

	/**
	 * Provides a mutator for a identity property.
	 */
	public interface IdentityMutator {

		/**
		 * Sets the identity for the name property.
		 * 
		 * @param aIdentity The identity to be stored by the name property.
		 */
		void setIdentity( String aIdentity );
	}

	/**
	 * Provides a builder method for a identity property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface IdentityBuilder<B extends IdentityBuilder<B>> {

		/**
		 * Sets the identity for the identity property.
		 * 
		 * @param aIdentity The identity to be stored by the identity property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withIdentity( String aIdentity );
	}

	/**
	 * Provides a identity property.
	 */
	public interface IdentityProperty extends IdentityAccessor, IdentityMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setIdentity(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aIdentity The {@link String} to set (via
		 *        {@link #setIdentity(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letIdentity( String aIdentity ) {
			setIdentity( aIdentity );
			return aIdentity;
		}
	}
}
