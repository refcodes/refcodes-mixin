// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a alias property.
 */
public interface AliasAccessor {

	/**
	 * Retrieves the alias from the alias property.
	 * 
	 * @return The alias stored by the alias property.
	 */
	String getAlias();

	/**
	 * Provides a mutator for a alias property.
	 */
	public interface AliasMutator {

		/**
		 * Sets the alias for the alias property.
		 * 
		 * @param aAlias The alias to be stored by the alias property.
		 */
		void setAlias( String aAlias );
	}

	/**
	 * Provides a builder method for a alias property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AliasBuilder<B extends AliasBuilder<B>> {

		/**
		 * Sets the alias for the alias property.
		 * 
		 * @param aAlias The alias to be stored by the alias property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAlias( String aAlias );
	}

	/**
	 * Provides a alias property.
	 */
	public interface AliasProperty extends AliasAccessor, AliasMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setAlias(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aAlias The {@link String} to set (via
		 *        {@link #setAlias(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letAlias( String aAlias ) {
			setAlias( aAlias );
			return aAlias;
		}
	}
}
