// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Implementation of the {@link BoxGrid} interface.
 * 
 * @param <T> The type of the grid elements for drawing a box grid, may be a
 *        char or a sprite or a byte array.
 */
public class BoxGridImpl<T> implements BoxGrid<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected T _leftEdge;
	protected T _topLeftEdge;
	protected T _dividerEdge;
	protected T _topDividerEdge;
	protected T _bottomDividerEdge;
	protected T _rightEdge;
	protected T _topRightEdge;
	protected T _bottomRightEdge;
	protected T _bottomLeftEdge;
	protected T _leftLine;
	protected T _dividerLine;
	protected T _rightLine;
	protected T _topLine;
	protected T _bottomLine;
	protected T _innerLine;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Default constructor to be used by sub-classes setting the member
	 * variables by themselves.
	 */
	protected BoxGridImpl() {}

	/**
	 * Constructs the {@link BoxGrid} instance.
	 *
	 * @param aTopLeftEdge the top left edge, for example "├".
	 * @param aTopLine The top line, for example "─" .
	 * @param aTopDividerEdge The top divider edge, for example "┬".
	 * @param aTopRightEdge the top right edge, for example "┐".
	 * @param aRightLine The right line, for example "│".
	 * @param aRightEdge The right edge, for example "┤".
	 * @param aBottomRightEdge the bottom right edge, for example "┘".
	 * @param aBottomLine The bottom line, for example "─".
	 * @param aBottomDividerEdge The bottom divider edge, for example "┴".
	 * @param aBottomLeftEdge the bottom left edge, for example "└".
	 * @param aLeftLine The left line, for example "│".
	 * @param aLeftEdge The top left edge, for example "├".
	 * @param aDividerLine the divider line, for example "│".
	 * @param aDividerEdge The divider edge, for example "┼".
	 * @param aInnerLine The inner line, for example "─".
	 */
	public BoxGridImpl( T aTopLeftEdge, T aTopLine, T aTopDividerEdge, T aTopRightEdge, T aRightLine, T aRightEdge, T aBottomRightEdge, T aBottomLine, T aBottomDividerEdge, T aBottomLeftEdge, T aLeftLine, T aLeftEdge, T aDividerLine, T aDividerEdge, T aInnerLine ) {
		_leftEdge = aLeftEdge;
		_topLeftEdge = aTopLeftEdge;
		_bottomLeftEdge = aBottomLeftEdge;
		_dividerEdge = aDividerEdge;
		_topDividerEdge = aTopDividerEdge;
		_bottomDividerEdge = aBottomDividerEdge;
		_rightEdge = aRightEdge;
		_topRightEdge = aTopRightEdge;
		_bottomRightEdge = aBottomRightEdge;
		_leftLine = aLeftLine;
		_dividerLine = aDividerLine;
		_rightLine = aRightLine;
		_topLine = aTopLine;
		_bottomLeftEdge = aBottomLeftEdge;
		_bottomDividerEdge = aBottomDividerEdge;
		_bottomRightEdge = aBottomRightEdge;
		_bottomLine = aBottomLine;
		_innerLine = aInnerLine;

	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getLeftEdge() {
		return _leftEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getTopLeftEdge() {
		return _topLeftEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getBottomLeftEdge() {
		return _bottomLeftEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getDividerEdge() {
		return _dividerEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getTopDividerEdge() {
		return _topDividerEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getBottomDividerEdge() {
		return _bottomDividerEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getRightEdge() {
		return _rightEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getTopRightEdge() {
		return _topRightEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getBottomRightEdge() {
		return _bottomRightEdge;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getLeftLine() {
		return _leftLine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getDividerLine() {
		return _dividerLine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getRightLine() {
		return _rightLine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getTopLine() {
		return _topLine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getBottomLine() {
		return _bottomLine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getInnerLine() {
		return _innerLine;
	}
}
