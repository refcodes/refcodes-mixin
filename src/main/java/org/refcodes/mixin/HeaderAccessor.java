// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a property for e.g. header/value-pair.
 *
 * @param <T> The type of the header to be used.
 */
public interface HeaderAccessor<T> {
	/**
	 * Retrieves the value from the header property.
	 * 
	 * @return The header stored by the header property.
	 */
	T getHeader();

	/**
	 * Provides a mutator for a header property.
	 * 
	 * @param <T> The type of the header property.
	 */
	public interface HeaderMutator<T> {

		/**
		 * Sets the header for the header property.
		 * 
		 * @param aHeader The header to be stored by the header property.
		 */
		void setHeader( T aHeader );
	}

	/**
	 * Provides a builder method for a header property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <T> the header type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HeaderBuilder<T, B extends HeaderBuilder<T, B>> {

		/**
		 * Sets the header for the header property.
		 * 
		 * @param aHeader The header to be stored by the header property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHeader( T aHeader );
	}

	/**
	 * Provides a header property.
	 * 
	 * @param <T> The type of the header property.
	 */
	public interface HeaderProperty<T> extends HeaderAccessor<T>, HeaderMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setHeader(Object)} and returns the very same value (getter).
		 * 
		 * @param aHeader The value to set (via {@link #setHeader(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letHeader( T aHeader ) {
			setHeader( aHeader );
			return aHeader;
		}
	}
}
