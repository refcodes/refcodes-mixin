// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a arguments property for e.g. key / arguments pair.
 *
 * @param <ARG> The type of the arguments to be used.
 */
public interface ArgumentsAccessor<ARG> {

	/**
	 * Retrieves the arguments from the arguments property.
	 * 
	 * @return The arguments stored by the arguments property.
	 */
	ARG[] getArguments();

	/**
	 * Extends the {@link ArgumentsAccessor} with a setter method.
	 * 
	 * @param <ARG> The type of the arguments property.
	 */
	public interface ArgumentsMutator<ARG> {

		/**
		 * Sets the arguments for the arguments property.
		 * 
		 * @param aArguments The arguments to be stored by the arguments
		 *        property.
		 */
		void setArguments( ARG[] aArguments );

	}

	/**
	 * Provides a builder method for a arguments property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <ARG> The type of the arguments to be used.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ArgumentsBuilder<ARG, B extends ArgumentsBuilder<ARG, B>> {

		/**
		 * Sets the arguments for the arguments property.
		 * 
		 * @param aArguments The arguments to be stored by the arguments
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withArguments( ARG[] aArguments );
	}

	/**
	 * Extends the {@link ArgumentsAccessor} with a setter method.
	 * 
	 * @param <ARG> The type of the arguments property.
	 */
	public interface ArgumentsProperty<ARG> extends ArgumentsAccessor<ARG>, ArgumentsMutator<ARG> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setArguments(Object[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aArguments The value to set (via
		 *        {@link #setArguments(Object[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ARG[] letArguments( ARG[] aArguments ) {
			setArguments( aArguments );
			return aArguments;
		}
	}
}
