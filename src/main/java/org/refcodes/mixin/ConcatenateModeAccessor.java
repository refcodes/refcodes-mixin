// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a {@link ConcatenateMode} property.
 */
public interface ConcatenateModeAccessor {

	/**
	 * Retrieves the {@link ConcatenateMode} from the {@link ConcatenateMode}
	 * property.
	 * 
	 * @return The {@link ConcatenateMode} stored by the {@link ConcatenateMode}
	 *         property.
	 */
	ConcatenateMode getConcatenateMode();

	/**
	 * Provides a mutator for a {@link ConcatenateMode} property.
	 */
	public interface ConcatenateModeMutator {

		/**
		 * Sets the {@link ConcatenateMode} for the {@link ConcatenateMode}
		 * property.
		 * 
		 * @param aConcatenateMode The {@link ConcatenateMode} to be stored by
		 *        the {@link ConcatenateMode} property.
		 */
		void setConcatenateMode( ConcatenateMode aConcatenateMode );
	}

	/**
	 * Provides a builder method for a {@link ConcatenateMode} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ConcatenateModeBuilder<B extends ConcatenateModeBuilder<B>> {

		/**
		 * Sets the {@link ConcatenateMode} for the {@link ConcatenateMode}
		 * property.
		 * 
		 * @param aConcatenateMode The {@link ConcatenateMode} to be stored by
		 *        the {@link ConcatenateMode} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withConcatenateMode( ConcatenateMode aConcatenateMode );
	}

	/**
	 * Provides a {@link ConcatenateMode} property.
	 */
	public interface ConcatenateModeProperty extends ConcatenateModeAccessor, ConcatenateModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ConcatenateMode}
		 * (setter) as of {@link #setConcatenateMode(ConcatenateMode)} and
		 * returns the very same value (getter).
		 * 
		 * @param aConcatenateMode The {@link ConcatenateMode} to set (via
		 *        {@link #setConcatenateMode(ConcatenateMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ConcatenateMode letConcatenateMode( ConcatenateMode aConcatenateMode ) {
			setConcatenateMode( aConcatenateMode );
			return aConcatenateMode;
		}
	}
}
