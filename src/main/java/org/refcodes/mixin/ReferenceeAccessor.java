// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a referencee property (as of the decorator pattern).
 *
 * @param <REFERENCEE> The type of the referencee to be used.
 */
public interface ReferenceeAccessor<REFERENCEE> {

	/**
	 * Retrieves the value from the referencee property.
	 * 
	 * @return The referencee stored by the referencee property.
	 */
	REFERENCEE getReferencee();

	/**
	 * Provides a mutator for a referencee property.
	 * 
	 * @param <REFERENCEE> The type of the referencee property.
	 */
	public interface ReferenceeMutator<REFERENCEE> {

		/**
		 * Sets the referencee for the referencee property.
		 * 
		 * @param aReferencee The referencee to be stored by the referencee
		 *        property.
		 */
		void setReferencee( REFERENCEE aReferencee );
	}

	/**
	 * Provides a builder method for a referencee property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <REFERENCEE> the referencee type.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReferenceeBuilder<REFERENCEE, B extends ReferenceeBuilder<REFERENCEE, B>> {

		/**
		 * Sets the referencee for the referencee property.
		 * 
		 * @param aReferencee The referencee to be stored by the referencee
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReferencee( REFERENCEE aReferencee );
	}

	/**
	 * Provides a referencee property.
	 * 
	 * @param <REFERENCEE> The type of the referencee property.
	 */
	public interface ReferenceeProperty<REFERENCEE> extends ReferenceeAccessor<REFERENCEE>, ReferenceeMutator<REFERENCEE> {

		/**
		 * Sets the given referencee (setter) as of
		 * {@link #setReferencee(Object)} and returns the very same referencee
		 * (getter). This method stores and passes through the given referencee,
		 * which is very useful for builder APIs.
		 * 
		 * @param aReferencee The referencee to set (via
		 *        {@link #setReferencee(Object)}).
		 * 
		 * @return Returns referencee passed for it to be used in conclusive
		 *         processing steps.
		 */
		default REFERENCEE letReferencee( REFERENCEE aReferencee ) {
			setReferencee( aReferencee );
			return aReferencee;
		}
	}
}
