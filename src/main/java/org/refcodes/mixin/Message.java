// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * A {@link Message} consists of the least required information to send a
 * message.
 * 
 * @param <HEADER> The type of the head of the {@link Message} describing the
 *        {@link Message} (e.g. recipients).
 * @param <BODY> The type of the payload being carried by the {@link Message}.
 */
public interface Message<HEADER, BODY> extends HeaderAccessor<HEADER>, BodyAccessor<BODY> {

	/**
	 * {@inheritDoc} The header describes the {@link Message}.
	 */
	@Override
	HEADER getHeader();

	/**
	 * {@inheritDoc} Retrieves the payload carried by the {@link Message}.
	 * 
	 * @return The according payload.
	 */
	@Override
	BODY getBody();

}
