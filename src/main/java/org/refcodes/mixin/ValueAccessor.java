// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a value property for e.g. key/value-pair.
 *
 * @param <V> The type of the value to be used.
 */
public interface ValueAccessor<V> {

	/**
	 * Retrieves the value from the value property.
	 * 
	 * @return The value stored by the value property.
	 */
	V getValue();

	/**
	 * Retrieves the value from the value property. If there is no value for the
	 * property, then the provided value is returned. Depending on the
	 * implementation, not present might mean <code>null</code> or empty as well
	 * (being "" for a {@link String}).
	 * 
	 * @param aValue In case the value to be retrieved is null, then the given
	 *        value is returned.
	 * 
	 * @return The value stored by the value property or the given value if the
	 *         stored value is null.
	 */
	default V getValueOr( V aValue ) {
		final V theValue = getValue();
		if ( theValue == null || ( theValue instanceof String && ( (String) theValue ).isEmpty() ) ) {
			return aValue;
		}
		return theValue;
	}

	/**
	 * Extends the {@link ValueAccessor} with a setter method.
	 * 
	 * @param <V> The type of the value property.
	 */
	public interface ValueMutator<V> {

		/**
		 * Sets the value for the value property.
		 * 
		 * @param aValue The value to be stored by the value property.
		 */
		void setValue( V aValue );

	}

	/**
	 * Provides a builder method for a the property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <V> the value type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ValueBuilder<V, B extends ValueBuilder<V, B>> {

		/**
		 * Sets the value for the the property.
		 * 
		 * @param aValue The value to be stored by the value property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withValue( V aValue );
	}

	/**
	 * Extends the {@link ValueAccessor} with a setter method.
	 * 
	 * @param <V> The type of the value property.
	 */
	public interface ValueProperty<V> extends ValueAccessor<V>, ValueMutator<V> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setValue(Object)} and returns the very same value (getter).
		 * 
		 * @param aValue The value to set (via {@link #setValue(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default V letValue( V aValue ) {
			setValue( aValue );
			return aValue;
		}
	}
}
