// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a adaptee property (as of the adapter pattern).
 *
 * @param <ADAPTEE> The type of the adaptee to be used.
 */
public interface AdapteeAccessor<ADAPTEE> {

	/**
	 * Retrieves the value from the adaptee property.
	 * 
	 * @return The adaptee stored by the adaptee property.
	 */
	ADAPTEE getAdaptee();

	/**
	 * Provides a mutator for a adaptee property.
	 * 
	 * @param <ADAPTEE> The type of the adaptee property.
	 */
	public interface AdapteeMutator<ADAPTEE> {

		/**
		 * Sets the adaptee for the adaptee property.
		 * 
		 * @param aAdaptee The adaptee to be stored by the adaptee property.
		 */
		void setAdaptee( ADAPTEE aAdaptee );
	}

	/**
	 * Provides a builder method for a adaptee property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <ADAPTEE> the adaptee type.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AdapteeBuilder<ADAPTEE, B extends AdapteeBuilder<ADAPTEE, B>> {

		/**
		 * Sets the adaptee for the adaptee property.
		 * 
		 * @param aAdaptee The adaptee to be stored by the adaptee property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAdaptee( ADAPTEE aAdaptee );
	}

	/**
	 * Provides a adaptee property.
	 * 
	 * @param <ADAPTEE> The type of the adaptee property.
	 */
	public interface AdapteeProperty<ADAPTEE> extends AdapteeAccessor<ADAPTEE>, AdapteeMutator<ADAPTEE> {

		/**
		 * Sets the given adaptee (setter) as of {@link #setAdaptee(Object)} and
		 * returns the very same adaptee (getter). This method stores and passes
		 * through the given adaptee, which is very useful for builder APIs.
		 * 
		 * @param aAdaptee The adaptee to set (via {@link #setAdaptee(Object)}).
		 * 
		 * @return Returns adaptee passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ADAPTEE letAdaptee( ADAPTEE aAdaptee ) {
			setAdaptee( aAdaptee );
			return aAdaptee;
		}
	}
}
