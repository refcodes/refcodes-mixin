// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.io.InputStream;

/**
 * Provides an accessor for an input stream property.
 */
public interface InputStreamAccessor {

	/**
	 * Retrieves the input stream from the input stream property.
	 * 
	 * @return The input stream stored by the input stream property.
	 */
	InputStream getInputStream();

	/**
	 * Provides a mutator for an input stream property.
	 */
	public interface InputStreamMutator {

		/**
		 * Sets the input stream for the input stream property.
		 * 
		 * @param aInputStream The input stream to be stored by the input stream
		 *        property.
		 */
		void setInputStream( InputStream aInputStream );
	}

	/**
	 * Provides a mutator for an input stream property.
	 * 
	 * @param <B> The builder which implements the {@link InputStreamBuilder}.
	 */
	public interface InputStreamBuilder<B extends InputStreamBuilder<?>> {

		/**
		 * Sets the input stream to use and returns this builder as of the
		 * Builder-Pattern.
		 * 
		 * @param aInputStream The input stream to be stored by the input stream
		 *        property.
		 * 
		 * @return This {@link InputStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withInputStream( InputStream aInputStream );
	}

	/**
	 * Provides an input stream property.
	 */
	public interface InputStreamProperty extends InputStreamAccessor, InputStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link InputStream}
		 * (setter) as of {@link #setInputStream(InputStream)} and returns the
		 * very same value (getter).
		 * 
		 * @param aInputStream The {@link InputStream} to set (via
		 *        {@link #setInputStream(InputStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default InputStream letInputStream( InputStream aInputStream ) {
			setInputStream( aInputStream );
			return aInputStream;
		}
	}
}
