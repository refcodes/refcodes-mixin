// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a secret property.
 */
public interface SecretAccessor {

	/**
	 * Retrieves the password (hash) from the secret property.
	 * 
	 * @return The password (hash) stored by the secret property.
	 */
	String getSecret();

	/**
	 * Provides a mutator for a secret property.
	 */
	public interface SecretMutator {

		/**
		 * Sets the password (hash) for the secret property.
		 * 
		 * @param aSecret The password (hash) to be stored by the name secret.
		 */
		void setSecret( String aSecret );
	}

	/**
	 * Provides a builder method for a secret property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SecretBuilder<B extends SecretBuilder<B>> {

		/**
		 * Sets the secret for the secret property.
		 * 
		 * @param aSecret The secret to be stored by the secret property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSecret( String aSecret );
	}

	/**
	 * Provides a secret property.
	 */
	public interface SecretProperty extends SecretAccessor, SecretMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setSecret(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aSecret The {@link String} to set (via
		 *        {@link #setSecret(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letSecret( String aSecret ) {
			setSecret( aSecret );
			return aSecret;
		}
	}
}
