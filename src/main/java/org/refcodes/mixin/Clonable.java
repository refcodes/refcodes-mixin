// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provide the {@link Object#clone()} method to interfaces!.
 */
public interface Clonable extends Cloneable {

	/**
	 * Makes the {@link Object}'s {@link Object#clone()} method available to
	 * this interface.
	 * 
	 * @return The cloned instance.
	 * 
	 * @throws CloneNotSupportedException Thrown to indicate that the
	 *         <code>clone</code> method in class <code>Object</code> has been
	 *         called to clone an object, but that the object's class does not
	 *         implement the <code>Cloneable</code> interface.
	 */
	Object clone() throws CloneNotSupportedException;

}
