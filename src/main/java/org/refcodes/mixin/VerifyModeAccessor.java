// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a verify mode property.
 */
public interface VerifyModeAccessor {

	/**
	 * Retrieves the verify mode from the verify mode property.
	 * 
	 * @return The verify mode stored by the verify mode property.
	 */
	VerifyMode getVerifyMode();

	/**
	 * Provides a mutator for a verify mode property.
	 */
	public interface VerifyModeMutator {

		/**
		 * Sets the verify mode for the verify mode property.
		 * 
		 * @param aVerifyMode The verify mode to be stored by the verify mode
		 *        property.
		 */
		void setVerifyMode( VerifyMode aVerifyMode );
	}

	/**
	 * Provides a builder method for a verify mode property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface VerifyModeBuilder<B extends VerifyModeBuilder<B>> {

		/**
		 * Sets the verify mode for the verify mode property.
		 * 
		 * @param aVerifyMode The verify mode to be stored by the verify mode
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withVerifyMode( VerifyMode aVerifyMode );
	}

	/**
	 * Provides a verify mode property.
	 */
	public interface VerifyModeProperty extends VerifyModeAccessor, VerifyModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link VerifyMode}
		 * (setter) as of {@link #setVerifyMode(VerifyMode)} and returns the
		 * very same value (getter).
		 * 
		 * @param aVerifyMode The {@link VerifyMode} to set (via
		 *        {@link #setVerifyMode(VerifyMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default VerifyMode letVerifyMode( VerifyMode aVerifyMode ) {
			setVerifyMode( aVerifyMode );
			return aVerifyMode;
		}
	}
}
