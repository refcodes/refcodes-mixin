// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a code property for e.g. key / code pair.
 *
 * @param <T> The type of the code to be used.
 */
public interface CodeAccessor<T> {

	/**
	 * Retrieves the code from the code property.
	 * 
	 * @return The code stored by the code property.
	 */
	T getCode();

	/**
	 * Extends the {@link CodeAccessor} with a setter method.
	 * 
	 * @param <T> The type of the code property.
	 */
	public interface CodeMutator<T> {

		/**
		 * Sets the code for the code property.
		 * 
		 * @param aCode The code to be stored by the code property.
		 */
		void setCode( T aCode );

	}

	/**
	 * Provides a builder method for a the property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <T> the value type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CodeBuilder<T, B extends CodeBuilder<T, B>> {

		/**
		 * Sets the code for the the property.
		 * 
		 * @param aCode The code to be stored by the code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCode( T aCode );
	}

	/**
	 * Extends the {@link CodeAccessor} with a setter method.
	 * 
	 * @param <T> The type of the code property.
	 */
	public interface CodeProperty<T> extends CodeAccessor<T>, CodeMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setCode(Object)} and returns the very same value (getter).
		 * 
		 * @param aCode The value to set (via {@link #setCode(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letCode( T aCode ) {
			setCode( aCode );
			return aCode;
		}
	}
}
