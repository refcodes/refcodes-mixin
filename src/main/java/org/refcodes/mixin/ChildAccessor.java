// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a child property.
 *
 * @param <T> The type of the child to be accessed.
 */
public interface ChildAccessor<T> {

	/**
	 * Retrieves the child from the child property.
	 * 
	 * @return The child stored by the child property.
	 */
	T getChild();

	/**
	 * Provides a mutator for a child property.
	 * 
	 * @param <T> The type of the child to be accessed.
	 */
	public interface ChildMutator<T> {

		/**
		 * Sets the child for the child property.
		 * 
		 * @param aChild The child to be stored by the child property.
		 */
		void setChild( T aChild );
	}

	/**
	 * Provides a builder method for a child property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <T> The type of the child to be accessed.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ChildBuilder<T, B extends ChildBuilder<T, B>> {

		/**
		 * Sets the child for the child property.
		 * 
		 * @param aChild The child to be stored by the child property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withChild( T aChild );
	}

	/**
	 * Provides a child property.
	 * 
	 * @param <T> The type of the child to be accessed.
	 */
	public interface ChildProperty<T> extends ChildAccessor<T>, ChildMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setChild(Object)} and returns the very same value (getter).
		 * 
		 * @param aChild The value to set (via {@link #setChild(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letChild( T aChild ) {
			setChild( aChild );
			return aChild;
		}
	}
}
