// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a Encrypt-Prefix property.
 */
public interface EncryptPrefixAccessor {

	/**
	 * Retrieves the Encrypt-Prefix from the Encrypt-Prefix property.
	 * 
	 * @return The Encrypt-Prefix stored by the Encrypt-Prefix property.
	 */
	String getEncryptPrefix();

	/**
	 * Provides a mutator for a Encrypt-Prefix property.
	 */
	public interface EncryptPrefixMutator {

		/**
		 * Sets the Encrypt-Prefix for the Encrypt-Prefix property.
		 * 
		 * @param aEncryptPrefix The Encrypt-Prefix to be stored by the
		 *        Encrypt-Prefix property.
		 */
		void setEncryptPrefix( String aEncryptPrefix );
	}

	/**
	 * Provides a builder method for a Encrypt-Prefix property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface EncryptPrefixBuilder<B extends EncryptPrefixBuilder<B>> {

		/**
		 * Sets the Encrypt-Prefix for the Encrypt-Prefix property.
		 * 
		 * @param aEncryptPrefix The Encrypt-Prefix to be stored by the
		 *        Encrypt-Prefix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withEncryptPrefix( String aEncryptPrefix );
	}

	/**
	 * Provides a Encrypt-Prefix property.
	 */
	public interface EncryptPrefixProperty extends EncryptPrefixAccessor, EncryptPrefixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setEncryptPrefix(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aEncryptPrefix The {@link String} to set (via
		 *        {@link #setEncryptPrefix(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letEncryptPrefix( String aEncryptPrefix ) {
			setEncryptPrefix( aEncryptPrefix );
			return aEncryptPrefix;
		}
	}
}
