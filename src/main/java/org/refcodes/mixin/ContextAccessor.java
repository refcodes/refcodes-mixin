// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a context property.
 *
 * @param <CTX> The context's type to be accessed.
 */
public interface ContextAccessor<CTX> {

	/**
	 * Retrieves the context from the context property.
	 * 
	 * @return The context stored by the context property.
	 */
	CTX getContext();

	/**
	 * Provides a mutator for a context property.
	 * 
	 * @param <CTX> The context's type to be accessed.
	 */
	public interface ContextMutator<CTX> {

		/**
		 * Sets the context for the context property.
		 * 
		 * @param aContext The context to be stored by the context property.
		 */
		void setContext( CTX aContext );
	}

	/**
	 * Provides a context property.
	 * 
	 * @param <CTX> The context's type to be accessed.
	 */
	public interface ContextProperty<CTX> extends ContextAccessor<CTX>, ContextMutator<CTX> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setContext(Object)} and returns the very same value (getter).
		 * 
		 * @param aContext The value to set (via {@link #setContext(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default CTX letContext( CTX aContext ) {
			setContext( aContext );
			return aContext;
		}
	}
}
