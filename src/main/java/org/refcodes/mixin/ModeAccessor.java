// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a mode property.
 *
 * @param <T> The type of the mode to be accessed.
 */
public interface ModeAccessor<T> {

	/**
	 * Retrieves the mode from the mode property.
	 * 
	 * @return The mode stored by the mode property.
	 */
	T getMode();

	/**
	 * Provides a mutator for a mode property.
	 * 
	 * @param <T> The type of the mode to be accessed.
	 */
	public interface ModeMutator<T> {

		/**
		 * Sets the mode for the mode property.
		 * 
		 * @param aMode The mode to be stored by the mode property.
		 */
		void setMode( T aMode );
	}

	/**
	 * Provides a mode property.
	 * 
	 * @param <T> The type of the mode to be accessed.
	 */
	public interface ModeProperty<T> extends ModeAccessor<T>, ModeMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setMode(Object)} and returns the very same value (getter).
		 * 
		 * @param aMode The value to set (via {@link #setMode(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letMode( T aMode ) {
			setMode( aMode );
			return aMode;
		}
	}
}
