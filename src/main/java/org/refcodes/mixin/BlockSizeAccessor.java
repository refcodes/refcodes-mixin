// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a block size property.
 */
public interface BlockSizeAccessor {

	/**
	 * Retrieves the block size from the block size property.
	 * 
	 * @return The block size stored by the block size property.
	 */
	int getBlockSize();

	/**
	 * Provides a mutator for a block size property.
	 */
	public interface BlockSizeMutator {

		/**
		 * Sets the block size for the block size property.
		 * 
		 * @param aBlockSize The block size to be stored by the block size
		 *        property.
		 */
		void setBlockSize( int aBlockSize );
	}

	/**
	 * Provides a builder method for a block size property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BlockSizeBuilder<B extends BlockSizeBuilder<B>> {

		/**
		 * Sets the block size for the block size property.
		 * 
		 * @param aBlockSize The block size to be stored by the block size
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBlockSize( int aBlockSize );
	}

	/**
	 * Provides a block size property.
	 */
	public interface BlockSizeProperty extends BlockSizeAccessor, BlockSizeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setBlockSize(int)} and returns the very same value (getter).
		 * 
		 * @param aBlockSize The integer to set (via
		 *        {@link #setBlockSize(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letBlockSize( int aBlockSize ) {
			setBlockSize( aBlockSize );
			return aBlockSize;
		}
	}
}
