// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a packet size property.
 */
public interface PacketSizeAccessor {

	/**
	 * Retrieves the packet size from the packet size property.
	 * 
	 * @return The packet size stored by the packet size property.
	 */
	int getPacketSize();

	/**
	 * Provides a mutator for a packet size property.
	 */
	public interface PacketSizeMutator {

		/**
		 * Sets the packet size for the packet size property.
		 * 
		 * @param aPacketSize The packet size to be stored by the packet size
		 *        property.
		 */
		void setPacketSize( int aPacketSize );
	}

	/**
	 * Provides a builder method for a packet size property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PacketSizeBuilder<B extends PacketSizeBuilder<B>> {

		/**
		 * Sets the packet size for the packet size property.
		 * 
		 * @param aPacketSize The packet size to be stored by the packet size
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPacketSize( int aPacketSize );
	}

	/**
	 * Provides a packet size property.
	 */
	public interface PacketSizeProperty extends PacketSizeAccessor, PacketSizeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setPacketSize(int)} and returns the very same value (getter).
		 * 
		 * @param aPacketSize The integer to set (via
		 *        {@link #setPacketSize(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letPacketSize( int aPacketSize ) {
			setPacketSize( aPacketSize );
			return aPacketSize;
		}
	}
}
