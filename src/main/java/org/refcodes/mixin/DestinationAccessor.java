// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open destination licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a destination property for e.g. destination of an
 * event.
 * 
 * @param <DEST> The type of the destination in question.
 */
public interface DestinationAccessor<DEST> {

	/**
	 * Retrieves the destination from the destination property.
	 * 
	 * @return The destination stored by the destination property.
	 */
	DEST getDestination();

	/**
	 * Provides a mutator for a destination property for e.g. destination of an
	 * event.
	 * 
	 * @param <DEST> The type of the destination in question.
	 */
	public interface DestinationMutator<DEST> {

		/**
		 * Sets the destination for the destination property.
		 * 
		 * @param aDestination The destination to be stored by the destination
		 *        property.
		 */
		void setDestination( DEST aDestination );

	}

	/**
	 * Provides a destination property for e.g. destination of an event.
	 * 
	 * @param <DEST> The type of the destination in question.
	 */
	public interface DestinationProperty<DEST> extends DestinationAccessor<DEST>, DestinationMutator<DEST> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setDestination(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aDestination The value to set (via
		 *        {@link #setDestination(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default DEST letDestination( DEST aDestination ) {
			setDestination( aDestination );
			return aDestination;
		}
	}
}
