// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a milliseconds property.
 */
public interface TimeMillisAccessor {

	/**
	 * Retrieves the milliseconds from the milliseconds property.
	 * 
	 * @return The milliseconds stored by the milliseconds property.
	 */
	int getTimeMillis();

	/**
	 * Provides a mutator for a milliseconds property.
	 */
	public interface TimeMillisMutator {

		/**
		 * Sets the milliseconds for the milliseconds property.
		 * 
		 * @param aMilliseconds The milliseconds to be stored by the
		 *        milliseconds property.
		 */
		void setTimeMillis( int aMilliseconds );
	}

	/**
	 * Provides a builder method for a milliseconds property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TimeMillisBuilder<B extends TimeMillisBuilder<B>> {

		/**
		 * Sets the milliseconds for the milliseconds property.
		 * 
		 * @param aMilliseconds The milliseconds to be stored by the
		 *        milliseconds property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTimeMillis( int aMilliseconds );
	}

	/**
	 * Provides a milliseconds property.
	 */
	public interface TimeMillisProperty extends TimeMillisAccessor, TimeMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setTimeMillis(int)} and returns the very same value (getter).
		 * 
		 * @param aMillis The integer to set (via {@link #setTimeMillis(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letTimeMillis( int aMillis ) {
			setTimeMillis( aMillis );
			return aMillis;
		}
	}
}
