// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * The {@link Lockable} interface defines methods to set the status of an object
 * to be locked or unlocked and to determine the status. Depending instances may
 * use {@link Lockable} instances to determine whether to modify an implementing
 * instance or keep it untouched. An implementing instance may throw the
 * <code>org.refcodes.exception.LockedRuntimeException</code> or the
 * <code>org.refcodes.exception.LockedException</code>, depending whether it
 * wants that exception to be checked or not.
 */
public interface Lockable {

	/**
	 * Locks the implementing instance.
	 */
	void lock();

	/**
	 * Unlocks the implementing instance.
	 */
	void unlock();

	/**
	 * Determines whether the implementing instance is locked.
	 * 
	 * @return True in case the implementing instance id is locked, else false-
	 */
	boolean isLocked();

}
