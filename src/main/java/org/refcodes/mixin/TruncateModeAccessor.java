// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a {@link TruncateMode} property.
 */
public interface TruncateModeAccessor {

	/**
	 * Retrieves the {@link TruncateMode} from the {@link TruncateMode}
	 * property.
	 * 
	 * @return The {@link TruncateMode} stored by the {@link TruncateMode}
	 *         property.
	 */
	TruncateMode getTruncateMode();

	/**
	 * Provides a mutator for a {@link TruncateMode} property.
	 */
	public interface TruncateModeMutator {

		/**
		 * Sets the {@link TruncateMode} for the {@link TruncateMode} property.
		 * 
		 * @param aTruncateMode The {@link TruncateMode} to be stored by the
		 *        {@link TruncateMode} property.
		 */
		void setTruncateMode( TruncateMode aTruncateMode );
	}

	/**
	 * Provides a builder method for a {@link TruncateMode} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TruncateModeBuilder<B extends TruncateModeBuilder<B>> {

		/**
		 * Sets the {@link TruncateMode} for the {@link TruncateMode} property.
		 * 
		 * @param aTruncateMode The {@link TruncateMode} to be stored by the
		 *        {@link TruncateMode} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTruncateMode( TruncateMode aTruncateMode );
	}

	/**
	 * Provides a {@link TruncateMode} property.
	 */
	public interface TruncateModeProperty extends TruncateModeAccessor, TruncateModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link TruncateMode}
		 * (setter) as of {@link #setTruncateMode(TruncateMode)} and returns the
		 * very same value (getter).
		 * 
		 * @param aTruncateMode The {@link TruncateMode} to set (via
		 *        {@link #setTruncateMode(TruncateMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TruncateMode letTruncateMode( TruncateMode aTruncateMode ) {
			setTruncateMode( aTruncateMode );
			return aTruncateMode;
		}
	}
}
