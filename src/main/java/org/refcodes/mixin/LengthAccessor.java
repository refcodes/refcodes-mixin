// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a length property.
 */
public interface LengthAccessor {

	/**
	 * Retrieves the length from the length property.
	 * 
	 * @return The length stored by the length property.
	 */
	int getLength();

	/**
	 * Provides a mutator for a length property.
	 */
	public interface LengthMutator {

		/**
		 * Sets the length for the length property.
		 * 
		 * @param aLength The length to be stored by the length property.
		 */
		void setLength( int aLength );
	}

	/**
	 * Provides a builder method for a length property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LengthBuilder<B extends LengthBuilder<B>> {

		/**
		 * Sets the length for the length property.
		 * 
		 * @param aLength The length to be stored by the length property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLength( int aLength );
	}

	/**
	 * Provides a length property.
	 */
	public interface LengthProperty extends LengthAccessor, LengthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setLength(int)} and returns the very same value (getter).
		 * 
		 * @param aLength The integer to set (via {@link #setLength(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letLength( int aLength ) {
			setLength( aLength );
			return aLength;
		}
	}
}
