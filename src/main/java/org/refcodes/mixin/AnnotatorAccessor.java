// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a annotator character property being the character
 * denoting (prefixing) an annotation's name.
 */
public interface AnnotatorAccessor {

	/**
	 * Retrieves the annotator character from the annotator character property
	 * being the character denoting (prefixing) an annotation's name.
	 * 
	 * @return The annotator character stored by the annotator character
	 *         property.
	 */
	char getAnnotator();

	/**
	 * Provides a mutator for a annotator character property.
	 */
	public interface AnnotatorMutator {

		/**
		 * Sets the annotator character for the annotator character property
		 * being the character denoting (prefixing) an annotation's name.
		 * 
		 * @param aAnnotator The annotator character to be stored by the
		 *        annotator character property.
		 */
		void setAnnotator( char aAnnotator );
	}

	/**
	 * Provides a builder method for a annotator character property being the
	 * character denoting (prefixing) an annotation's name and returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AnnotatorBuilder<B extends AnnotatorBuilder<B>> {

		/**
		 * Sets the annotator character for the annotator character property
		 * being the character denoting (prefixing) an annotation's name.
		 * 
		 * @param aAnnotator The annotator character to be stored by the
		 *        annotator character property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAnnotator( char aAnnotator );
	}

	/**
	 * Provides a annotator character property being the character denoting
	 * (prefixing) an annotation's name.
	 */
	public interface AnnotatorProperty extends AnnotatorAccessor, AnnotatorMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setAnnotator(char)} and returns the very same value (getter).
		 * 
		 * @param aAnnotator The character to set (via
		 *        {@link #setAnnotator(char)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default char letAnnotator( char aAnnotator ) {
			setAnnotator( aAnnotator );
			return aAnnotator;
		}
	}
}
