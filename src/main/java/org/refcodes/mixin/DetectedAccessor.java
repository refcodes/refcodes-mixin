// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a detected property.
 */
public interface DetectedAccessor {

	/**
	 * Retrieves the detected status from the detected property.
	 * 
	 * @return The detected status stored by the detected property.
	 */
	boolean isDetected();

	/**
	 * Provides a mutator for a detected property.
	 */
	public interface DetectedMutator {

		/**
		 * Sets the detected status for the detected property.
		 * 
		 * @param isDetected The detected status to be stored by the detected
		 *        property.
		 */
		void setDetected( boolean isDetected );
	}

	/**
	 * Provides a builder for a detected property.
	 *
	 * @param <B> the generic type
	 */
	public interface DetectedBuilder<B extends DetectedBuilder<B>> {

		/**
		 * Builder method for the {@link DetectedMutator#setDetected(boolean)}
		 * method.
		 * 
		 * @param isDetected The detected status to be stored by the detected
		 *        property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withDetected( boolean isDetected );
	}

	/**
	 * Provides a detected property.
	 */
	public interface DetectedProperty extends DetectedAccessor, DetectedMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setDetected(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isDetected The boolean to set (via
		 *        {@link #setDetected(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letDetected( boolean isDetected ) {
			setDetected( isDetected );
			return isDetected;
		}
	}
}
