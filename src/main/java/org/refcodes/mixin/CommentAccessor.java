// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a comment property.
 */
public interface CommentAccessor {

	/**
	 * Retrieves the comment from the comment property.
	 * 
	 * @return The comment stored by the comment property.
	 */
	String getComment();

	/**
	 * Provides a mutator for a comment property.
	 */
	public interface CommentMutator {

		/**
		 * Sets the comment for the comment property.
		 * 
		 * @param aComment The comment to be stored by the comment property.
		 */
		void setComment( String aComment );
	}

	/**
	 * Provides a builder method for a comment property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CommentBuilder<B extends CommentBuilder<B>> {

		/**
		 * Sets the comment for the comment property.
		 * 
		 * @param aComment The comment to be stored by the comment property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withComment( String aComment );
	}

	/**
	 * Provides a comment property.
	 */
	public interface CommentProperty extends CommentAccessor, CommentMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setComment(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aComment The {@link String} to set (via
		 *        {@link #setComment(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letComment( String aComment ) {
			setComment( aComment );
			return aComment;
		}
	}
}
