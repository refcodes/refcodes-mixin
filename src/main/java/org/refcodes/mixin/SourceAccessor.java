// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a source property for e.g. source of an event.
 * 
 * @param <SRC> The type of the source in question.
 */
public interface SourceAccessor<SRC> {

	/**
	 * Retrieves the source from the source property.
	 * 
	 * @return The source stored by the source property.
	 */
	SRC getSource();

	/**
	 * Provides a mutator for a source property for e.g. source of an event.
	 * 
	 * @param <SRC> The type of the source in question.
	 */
	public interface SourceMutator<SRC> {

		/**
		 * Sets the source for the source property.
		 * 
		 * @param aSource The source to be stored by the source property.
		 */
		void setSource( SRC aSource );
	}

	/**
	 * Provides a builder method for a source property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <SRC> The type of the source in question.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SourceBuilder<SRC, B extends SourceBuilder<SRC, B>> {

		/**
		 * Sets the source for the source property.
		 * 
		 * @param aSource The source to be stored by the source property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSource( SRC aSource );
	}

	/**
	 * Provides a source property for e.g. source of an event.
	 * 
	 * @param <SRC> The type of the source in question.
	 */
	public interface SourceProperty<SRC> extends SourceAccessor<SRC>, SourceMutator<SRC> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setSource(Object)} and returns the very same value (getter).
		 * 
		 * @param aSource The value to set (via {@link #setSource(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SRC letSource( SRC aSource ) {
			setSource( aSource );
			return aSource;
		}
	}
}
