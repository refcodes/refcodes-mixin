// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a request property for e.g. key / request pair.
 *
 * @param <REQ> The type of the request to be used.
 */
public interface RequestAccessor<REQ> {

	/**
	 * Retrieves the request from the request property.
	 * 
	 * @return The request stored by the request property.
	 */
	REQ getRequest();

	/**
	 * Extends the {@link RequestAccessor} with a setter method.
	 * 
	 * @param <REQ> The type of the request property.
	 */
	public interface RequestMutator<REQ> {

		/**
		 * Sets the request for the request property.
		 * 
		 * @param aRequest The request to be stored by the request property.
		 */
		void setRequest( REQ aRequest );

	}

	/**
	 * Provides a builder method for a request property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <REQ> The type of the request to be used.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RequestBuilder<REQ, B extends RequestBuilder<REQ, B>> {

		/**
		 * Sets the request for the request property.
		 * 
		 * @param aRequest The request to be stored by the request property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRequest( String aRequest );
	}

	/**
	 * Extends the {@link RequestAccessor} with a setter method.
	 * 
	 * @param <REQ> The type of the request property.
	 */
	public interface RequestProperty<REQ> extends RequestAccessor<REQ>, RequestMutator<REQ> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setRequest(Object)} and returns the very same value (getter).
		 * 
		 * @param aRequest The value to set (via {@link #setRequest(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default REQ letRequest( REQ aRequest ) {
			setRequest( aRequest );
			return aRequest;
		}
	}
}
