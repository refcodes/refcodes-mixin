// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a children property.
 *
 * @param <T> The type of the children to be accessed.
 */
public interface ChildrenAccessor<T> {

	/**
	 * Retrieves the children from the children property.
	 * 
	 * @return The children stored by the children property.
	 */
	T getChildren();

	/**
	 * Provides a mutator for a children property.
	 * 
	 * @param <T> The type of the children to be accessed.
	 */
	public interface ChildrenMutator<T> {

		/**
		 * Sets the children for the children property.
		 * 
		 * @param aChildren The children to be stored by the children property.
		 */
		void setChildren( T aChildren );
	}

	/**
	 * Provides a builder method for a children property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <T> The type of the children to be accessed.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ChildrenBuilder<T, B extends ChildrenBuilder<T, B>> {

		/**
		 * Sets the children for the children property.
		 * 
		 * @param aChildren The children to be stored by the children property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withChildren( T aChildren );
	}

	/**
	 * Provides a children property.
	 * 
	 * @param <T> The type of the children to be accessed.
	 */
	public interface ChildrenProperty<T> extends ChildrenAccessor<T>, ChildrenMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setChildren(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aChildren The value to set (via {@link #setChildren(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letChildren( T aChildren ) {
			setChildren( aChildren );
			return aChildren;
		}
	}
}
