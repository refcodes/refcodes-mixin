// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a empty property.
 */
public interface EmptyAccessor {

	/**
	 * Retrieves the empty status from the empty property.
	 * 
	 * @return The empty status stored by the empty property.
	 */
	boolean isEmpty();

	/**
	 * Provides a mutator for a empty property.
	 */
	public interface EmptyMutator {

		/**
		 * Sets the empty status for the empty property.
		 * 
		 * @param isEmpty The empty status to be stored by the empty property.
		 */
		void setEmpty( boolean isEmpty );
	}

	/**
	 * Provides a builder for a empty property.
	 *
	 * @param <B> the generic type
	 */
	public interface EmptyBuilder<B extends EmptyBuilder<B>> {

		/**
		 * Builder method for the {@link EmptyMutator#setEmpty(boolean)} method.
		 * 
		 * @param isEmpty The empty status to be stored by the empty property.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		B withEmpty( boolean isEmpty );
	}

	/**
	 * Provides a empty property.
	 */
	public interface EmptyProperty extends EmptyAccessor, EmptyMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setEmpty(boolean)} and returns the very same value (getter).
		 * 
		 * @param isEmpty The boolean to set (via {@link #setEmpty(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letEmpty( boolean isEmpty ) {
			setEmpty( isEmpty );
			return isEmpty;
		}
	}
}
