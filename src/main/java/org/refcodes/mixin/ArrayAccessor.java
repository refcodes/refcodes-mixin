// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a array property.
 *
 * @param <T> The type of the array to be accessed. In order to support
 *        primitive array types, the type must be an array type.
 */
public interface ArrayAccessor<T> {

	/**
	 * Retrieves the array from the array property.
	 * 
	 * @return The array stored by the array property.
	 */
	T getArray();

	/**
	 * Provides a mutator for a array property.
	 * 
	 * @param <T> The type of the array to be accessed.
	 */
	public interface ArrayMutator<T> {

		/**
		 * Sets the array for the array property.
		 * 
		 * @param aArray The array to be stored by the array property.
		 */
		void setArray( T aArray );
	}

	/**
	 * Provides a builder method for a array property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <T> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ArrayBuilder<T, B extends ArrayBuilder<T, B>> {

		/**
		 * Sets the array for the array property.
		 * 
		 * @param aArray The array to be stored by the array property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withArray( T aArray );
	}

	/**
	 * Provides a array property.
	 * 
	 * @param <T> The type of the array to be accessed.
	 */
	public interface ArrayProperty<T> extends ArrayAccessor<T>, ArrayMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setArray(Object)} and returns the very same value (getter).
		 * 
		 * @param aArray The value to set (via {@link #setArray(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letArray( T aArray ) {
			setArray( aArray );
			return aArray;
		}
	}
}
