// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a timeout in milliseconds property.
 */
public interface TimeoutMillisAccessor {

	/**
	 * The timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	long getTimeoutMillis();

	/**
	 * Provides a mutator for a timeout in milliseconds property.
	 */
	public interface TimeoutMillisMutator {

		/**
		 * The timeout attribute in milliseconds.
		 * 
		 * @param aTimeoutMillis An long integer with the timeout in
		 *        milliseconds.
		 */
		void setTimeoutMillis( long aTimeoutMillis );
	}

	/**
	 * Provides a builder method for a the property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TimeoutMillisBuilder<B extends TimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the the property.
		 * 
		 * @param aTimeoutMillis The timeout in milliseconds to be stored by the
		 *        read timeout property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTimeoutMillis( long aTimeoutMillis );
	}

	/**
	 * Provides a timeout in milliseconds property.
	 */
	public interface TimeoutMillisProperty extends TimeoutMillisAccessor, TimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setTimeoutMillis(long)} and returns the very same value
		 * (getter).
		 * 
		 * @param aTimeoutMillis The long to set (via
		 *        {@link #setTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letTimeoutMillis( long aTimeoutMillis ) {
			setTimeoutMillis( aTimeoutMillis );
			return aTimeoutMillis;
		}
	}
}
