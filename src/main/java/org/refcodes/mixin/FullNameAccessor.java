// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a full name property.
 */
public interface FullNameAccessor {

	/**
	 * Retrieves the full name from the full name property.
	 * 
	 * @return The full name stored by the full name property.
	 */
	String getFullName();

	/**
	 * Provides a mutator for a full name property.
	 */
	public interface FullNameMutator {

		/**
		 * Sets the full name for the full name property.
		 * 
		 * @param aFullName The full name to be stored by the full name
		 *        property.
		 */
		void setFullName( String aFullName );
	}

	/**
	 * Provides a builder method for a full name property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FullNameBuilder<B extends FullNameBuilder<B>> {

		/**
		 * Sets the full name for the full name property.
		 * 
		 * @param aFullName The full name to be stored by the full name
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFullName( String aFullName );
	}

	/**
	 * Provides a full name property.
	 */
	public interface FullNameProperty extends FullNameAccessor, FullNameMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setFullName(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aFullName The {@link String} to set (via
		 *        {@link #setFullName(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letFullName( String aFullName ) {
			setFullName( aFullName );
			return aFullName;
		}
	}
}
