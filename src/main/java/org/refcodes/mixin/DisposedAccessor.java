// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import org.refcodes.mixin.DomainAccessor.DomainBuilder;

/**
 * Provides an accessor for a dispose property. If the
 * {@link Disposable#dispose()} of the interface {@link Disposable} has not been
 * called, then false is returned, else true is returned. The return state has
 * to be implemented by the programmer using this interface.
 */
public interface DisposedAccessor {

	/**
	 * Returns the disposed status for the disposed property.
	 * 
	 * @return The disposed status to be stored by the disposed property.
	 */
	boolean isDisposed();

	/**
	 * Provides a mutator for a dispose property.
	 */
	public interface DisposedMutator {

		/**
		 * Sets the disposed status for the disposed property.
		 * 
		 * @param isDisposed The disposed status to be stored by the disposed
		 *        property.
		 */
		void setDisposed( boolean isDisposed );
	}

	/**
	 * Provides a mutator for an domain property.
	 * 
	 * @param <B> The builder which implements the {@link DomainBuilder}.
	 */
	public interface DisposedBuilder<B extends DisposedBuilder<?>> {

		/**
		 * Sets the disposed status for the disposed property.
		 * 
		 * @param isDisposed The disposed status to be stored by the disposed
		 *        property.
		 * 
		 * @return This {@link DisposedBuilder} instance to continue
		 *         configuration.
		 */
		B withDisposed( boolean isDisposed );
	}

	/**
	 * Provides a mutator for a dispose property.
	 */
	public interface DisposedProperty extends DisposedAccessor, DisposedMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setDisposed(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isDisposed The boolean to set (via
		 *        {@link #setDisposed(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letDisposed( boolean isDisposed ) {
			setDisposed( isDisposed );
			return isDisposed;
		}
	}
}
