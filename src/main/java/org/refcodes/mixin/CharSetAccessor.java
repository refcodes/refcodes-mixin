// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a char set property.
 */
public interface CharSetAccessor {

	/**
	 * Retrieves the charSet from the char set property.
	 * 
	 * @return The charSet stored by the char set property.
	 */
	char[] getCharSet();

	/**
	 * Provides a mutator for a char set property.
	 */
	public interface CharSetMutator {

		/**
		 * Sets the charSet for the char set property.
		 * 
		 * @param aCharSet The charSet to be stored by the char set property.
		 */
		void setCharSet( char[] aCharSet );
	}

	/**
	 * Provides a builder method for a char set property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CharSetBuilder<B extends CharSetBuilder<B>> {

		/**
		 * Sets the charSet for the char set property.
		 * 
		 * @param aCharSet The charSet to be stored by the char set property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCharSet( char[] aCharSet );
	}

	/**
	 * Provides a char set property.
	 */
	public interface CharSetProperty extends CharSetAccessor, CharSetMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setCharSet(char[])} and returns the very same value (getter).
		 * 
		 * @param aCharSet The character to set (via
		 *        {@link #setCharSet(char[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default char[] letCharSet( char[] aCharSet ) {
			setCharSet( aCharSet );
			return aCharSet;
		}
	}
}
