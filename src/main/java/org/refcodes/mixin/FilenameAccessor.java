// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a filename property.
 */
public interface FilenameAccessor {

	/**
	 * Retrieves the filename from the filename property.
	 * 
	 * @return The filename stored by the filename property.
	 */
	String getFilename();

	/**
	 * Provides a mutator for a filename property.
	 */
	public interface FilenameMutator {

		/**
		 * Sets the filename for the filename property.
		 * 
		 * @param aFilename The filename to be stored by the filename property.
		 */
		void setFilename( String aFilename );
	}

	/**
	 * Provides a builder method for a filename property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FilenameBuilder<B extends FilenameBuilder<B>> {

		/**
		 * Sets the filename for the filename property.
		 * 
		 * @param aFilename The filename to be stored by the filename property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFilename( String aFilename );
	}

	/**
	 * Provides a filename property.
	 */
	public interface FilenameProperty extends FilenameAccessor, FilenameMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setFilename(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aFilename The {@link String} to set (via
		 *        {@link #setFilename(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letFilename( String aFilename ) {
			setFilename( aFilename );
			return aFilename;
		}
	}
}
