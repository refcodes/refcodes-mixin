// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a token property.
 */
public interface TokenAccessor {

	/**
	 * Retrieves the token from the token property.
	 * 
	 * @return The token stored by the token property.
	 */
	String getToken();

	/**
	 * Provides a mutator for a token property.
	 */
	public interface TokenMutator {

		/**
		 * Sets the token for the token property.
		 * 
		 * @param aToken The token to be stored by the token property.
		 */
		void setToken( String aToken );
	}

	/**
	 * Provides a builder method for a token property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TokenBuilder<B extends TokenBuilder<B>> {

		/**
		 * Sets the token for the token property.
		 * 
		 * @param aToken The token to be stored by the token property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withToken( String aToken );
	}

	/**
	 * Provides a token property.
	 */
	public interface TokenProperty extends TokenAccessor, TokenMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setToken(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aToken The {@link String} to set (via
		 *        {@link #setToken(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letToken( String aToken ) {
			setToken( aToken );
			return aToken;
		}
	}
}
