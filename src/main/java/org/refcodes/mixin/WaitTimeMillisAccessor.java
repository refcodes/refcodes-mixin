// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a wait time in milliseconds property.
 */
public interface WaitTimeMillisAccessor {

	/**
	 * The wait time attribute in milliseconds.
	 * 
	 * @return An integer with the wait time in milliseconds.
	 */
	long getWaitTimeMillis();

	/**
	 * Provides a mutator for a wait time in milliseconds property.
	 */
	public interface WaitTimeMillisMutator {

		/**
		 * The wait time attribute in milliseconds.
		 * 
		 * @param aWaitTimeMillis An integer with the wait time in milliseconds.
		 */
		void setWaitTimeMillis( long aWaitTimeMillis );
	}

	/**
	 * Provides a builder method for a wait time attribute in milliseconds
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface WaitTimeMillisBuilder<B extends WaitTimeMillisBuilder<B>> {

		/**
		 * Builder method for the wait time attribute in milliseconds.
		 * 
		 * @param aWaitTimeMillis An integer with the wait time in milliseconds.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withWaitTimeMillis( int aWaitTimeMillis );
	}

	/**
	 * Provides a wait time in milliseconds property.
	 */
	public interface WaitTimeMillisProperty extends WaitTimeMillisAccessor, WaitTimeMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setWaitTimeMillis(long)} and returns the very same value
		 * (getter).
		 * 
		 * @param aWaitTimeMillis The long to set (via
		 *        {@link #setWaitTimeMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letWaitTimeMillis( long aWaitTimeMillis ) {
			setWaitTimeMillis( aWaitTimeMillis );
			return aWaitTimeMillis;
		}
	}
}
