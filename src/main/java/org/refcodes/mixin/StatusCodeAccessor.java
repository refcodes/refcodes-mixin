// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a status code property for e.g. key / status code pair.
 *
 * @param <SC> The type of the status code to be used.
 */
public interface StatusCodeAccessor<SC> {

	/**
	 * Retrieves the status code from the status code property.
	 * 
	 * @return The status code stored by the status code property.
	 */
	SC getStatusCode();

	/**
	 * Extends the {@link StatusCodeAccessor} with a setter method.
	 * 
	 * @param <SC> The type of the status code property.
	 */
	public interface StatusCodeMutator<SC> {

		/**
		 * Sets the status code for the status code property.
		 * 
		 * @param aStatusCode The status code to be stored by the status code
		 *        property.
		 */
		void setStatusCode( SC aStatusCode );

	}

	/**
	 * Provides a builder method for a status code property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <SC> The type of the status code to be used.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface StatusCodeBuilder<SC, B extends StatusCodeBuilder<SC, B>> {

		/**
		 * Sets the status code for the status code property.
		 * 
		 * @param aStatusCode The status code to be stored by the status code
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withStatusCode( String aStatusCode );
	}

	/**
	 * Extends the {@link StatusCodeAccessor} with a setter method.
	 * 
	 * @param <SC> The type of the status code property.
	 */
	public interface StatusCodeProperty<SC> extends StatusCodeAccessor<SC>, StatusCodeMutator<SC> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setStatusCode(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aStatusCode The value to set (via
		 *        {@link #setStatusCode(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SC letStatusCode( SC aStatusCode ) {
			setStatusCode( aStatusCode );
			return aStatusCode;
		}
	}
}
