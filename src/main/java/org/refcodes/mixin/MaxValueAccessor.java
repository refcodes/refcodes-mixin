// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides access to a maximum value property for e.g. key/maximum value-pair.
 *
 * @param <V> The type of the maximum value to be used.
 */
public interface MaxValueAccessor<V> {

	/**
	 * Retrieves the maximum value from the maximum value property.
	 * 
	 * @return The maximum value stored by the maximum value property.
	 */
	V getMaxValue();

	/**
	 * Extends the {@link MaxValueAccessor} with a setter method.
	 * 
	 * @param <V> The type of the maximum value property.
	 */
	public interface MaxValueMutator<V> {

		/**
		 * Sets the maximum value for the maximum value property.
		 * 
		 * @param aMaxValue The maximum value to be stored by the maximum value
		 *        property.
		 */
		void setMaxValue( V aMaxValue );
	}

	/**
	 * Provides a builder method for a the property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <V> the value type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MaxValueBuilder<V, B extends MaxValueBuilder<V, B>> {

		/**
		 * Sets the maximum value for the the property.
		 * 
		 * @param aMaxValue The maximum value to be stored by the maximum value
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxValue( V aMaxValue );
	}

	/**
	 * Extends the {@link MaxValueAccessor} with a setter method.
	 * 
	 * @param <V> The type of the maximum value property.
	 */
	public interface MaxValueProperty<V> extends MaxValueAccessor<V>, MaxValueMutator<V> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setMaxValue(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aMaxValue The value to set (via {@link #setMaxValue(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default V letMaxValue( V aMaxValue ) {
			setMaxValue( aMaxValue );
			return aMaxValue;
		}
	}
}
