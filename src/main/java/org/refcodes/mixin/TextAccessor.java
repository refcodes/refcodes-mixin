// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a text property.
 */
public interface TextAccessor {

	/**
	 * Retrieves the text from the text property.
	 * 
	 * @return The text stored by the text property.
	 */
	String getText();

	/**
	 * Provides a mutator for a text property.
	 */
	public interface TextMutator {

		/**
		 * Sets the text for the text property.
		 * 
		 * @param aText The text to be stored by the text property.
		 */
		void setText( String aText );
	}

	/**
	 * Provides a builder method for a text property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TextBuilder<B extends TextBuilder<B>> {

		/**
		 * Sets the text for the text property.
		 * 
		 * @param aText The text to be stored by the text property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withText( String aText );
	}

	/**
	 * Provides a text property.
	 */
	public interface TextProperty extends TextAccessor, TextMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setText(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aText The {@link String} to set (via
		 *        {@link #setText(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letText( String aText ) {
			setText( aText );
			return aText;
		}
	}
}
