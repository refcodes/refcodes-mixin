// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a filename suffix property. A filename suffix also
 * contains the dot "." separating the suffix from the name, e.g. ".ini".
 */
public interface FilenameSuffixAccessor {

	/**
	 * Retrieves the filename suffix from the filename suffix property. A
	 * filename suffix also contains the dot "." separating the suffix from the
	 * name, e.g. ".ini".
	 * 
	 * @return The filename suffix stored by the filename suffix property.
	 */
	String getFilenameSuffix();

	/**
	 * Provides a mutator for a filename suffix property.
	 */
	public interface FilenameSuffixMutator {

		/**
		 * Sets the filename suffix for the filename suffix property. A filename
		 * suffix also contains the dot "." separating the suffix from the name,
		 * e.g. ".ini".
		 * 
		 * @param aFilenameSuffix The filename suffix to be stored by the
		 *        filename suffix property.
		 */
		void setFilenameSuffix( String aFilenameSuffix );
	}

	/**
	 * Provides a builder method for a filename suffix property returning the
	 * builder for applying multiple build operations. A filename suffix also
	 * contains the dot "." separating the suffix from the name, e.g. ".ini".
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FilenameSuffixBuilder<B extends FilenameSuffixBuilder<B>> {

		/**
		 * Sets the filename suffix for the filename suffix property. A filename
		 * suffix also contains the dot "." separating the suffix from the name,
		 * e.g. ".ini".
		 * 
		 * @param aFilenameSuffix The filename suffix to be stored by the
		 *        filename suffix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFilenameSuffix( String aFilenameSuffix );
	}

	/**
	 * Provides a filename suffix property. A filename suffix also contains the
	 * dot "." separating the suffix from the name, e.g. ".ini".
	 */
	public interface FilenameSuffixProperty extends FilenameSuffixAccessor, FilenameSuffixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setFilenameSuffix(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aFilenameSuffix The {@link String} to set (via
		 *        {@link #setFilenameSuffix(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letFilenameSuffix( String aFilenameSuffix ) {
			setFilenameSuffix( aFilenameSuffix );
			return aFilenameSuffix;
		}
	}
}
