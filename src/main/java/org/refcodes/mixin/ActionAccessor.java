// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a action property.
 * 
 * @param <A> The type of the action property.
 */
public interface ActionAccessor<A> {

	/**
	 * Retrieves the action from the action property.
	 * 
	 * @return The action stored by the action property.
	 */
	A getAction();

	/**
	 * Provides a mutator for a action property.
	 * 
	 * @param <A> The type of the action property.
	 */
	public interface ActionMutator<A> {

		/**
		 * Sets the action for the action property.
		 * 
		 * @param aAction The action to be stored by the action property.
		 */
		void setAction( A aAction );
	}

	/**
	 * Provides a builder method for a action property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <A> The type of the action property.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ActionBuilder<A, B extends ActionBuilder<A, B>> {

		/**
		 * Sets the action for the action property.
		 * 
		 * @param aAction The action to be stored by the action property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAction( A aAction );
	}

	/**
	 * Provides a action property.
	 * 
	 * @param <A> The type of the action property.
	 */
	public interface ActionProperty<A> extends ActionAccessor<A>, ActionMutator<A> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setAction(Object)} and returns the very same value (getter).
		 * 
		 * @param aAction The value to set (via {@link #setAction(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default A letAction( A aAction ) {
			setAction( aAction );
			return aAction;
		}
	}
}
