// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a filename extension property. A filename extensions
 * does not(!) contain the dot "." separating the extension from the name, e.g.
 * "ini" (instead of ".ini").
 */
public interface FilenameExtensionAccessor {

	/**
	 * Retrieves the filename extension from the filename extension property. A
	 * filename extensions does not(!) contain the dot "." separating the
	 * extension from the name, e.g. "ini" (instead of ".ini").
	 * 
	 * @return The filename extension stored by the filename extension property.
	 */
	String getFilenameExtension();

	/**
	 * Provides a mutator for a filename extension property.
	 */
	public interface FilenameExtensionMutator {

		/**
		 * Sets the filename extension for the filename extension property. A
		 * filename extensions does not(!) contain the dot "." separating the
		 * extension from the name, e.g. "ini" (instead of ".ini").
		 * 
		 * @param aFilenameExtension The filename extension to be stored by the
		 *        filename extension property.
		 */
		void setFilenameExtension( String aFilenameExtension );
	}

	/**
	 * Provides a builder method for a filename extension property returning the
	 * builder for applying multiple build operations. A filename extensions
	 * does not(!) contain the dot "." separating the extension from the name,
	 * e.g. "ini" (instead of ".ini").
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FilenameExtensionBuilder<B extends FilenameExtensionBuilder<B>> {

		/**
		 * Sets the filename extension for the filename extension property. A
		 * filename extensions does not(!) contain the dot "." separating the
		 * extension from the name, e.g. "ini" (instead of ".ini").
		 * 
		 * @param aFilenameExtension The filename extension to be stored by the
		 *        filename extension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFilenameExtension( String aFilenameExtension );
	}

	/**
	 * Provides a filename extension property. A filename extensions does not(!)
	 * contain the dot "." separating the extension from the name, e.g. "ini"
	 * (instead of ".ini").
	 */
	public interface FilenameExtensionProperty extends FilenameExtensionAccessor, FilenameExtensionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setFilenameExtension(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aFilenameExtension The {@link String} to set (via
		 *        {@link #setFilenameExtension(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letFilenameExtension( String aFilenameExtension ) {
			setFilenameExtension( aFilenameExtension );
			return aFilenameExtension;
		}
	}
}
