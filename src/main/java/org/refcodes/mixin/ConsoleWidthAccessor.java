// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a console width property.
 */
public interface ConsoleWidthAccessor {

	/**
	 * Retrieves the console width from the console width property.
	 * 
	 * @return The console width stored by the console width property.
	 */
	int getConsoleWidth();

	/**
	 * Provides a mutator for a console width property.
	 */
	public interface ConsoleWidthMutator {

		/**
		 * Sets the console width for the console width property.
		 * 
		 * @param aConsoleWidth The console width to be stored by the console
		 *        width property.
		 */
		void setConsoleWidth( int aConsoleWidth );
	}

	/**
	 * Provides a builder method for a console width property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ConsoleWidthBuilder<B extends ConsoleWidthBuilder<B>> {

		/**
		 * Sets the console width for the console width property.
		 * 
		 * @param aConsoleWidth The console width to be stored by the console
		 *        width property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withConsoleWidth( int aConsoleWidth );
	}

	/**
	 * Provides a console width property.
	 */
	public interface ConsoleWidthProperty extends ConsoleWidthAccessor, ConsoleWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given width (setter) as of
		 * {@link #setConsoleWidth(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aConsoleWidth The width to set (via
		 *        {@link #setConsoleWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letConsoleWidth( int aConsoleWidth ) {
			setConsoleWidth( aConsoleWidth );
			return aConsoleWidth;
		}
	}
}
