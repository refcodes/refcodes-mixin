// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for to a Meta-Data property.
 *
 * @param <MD> The type of the Meta-Data to be used.
 */
public interface MetaDataAccessor<MD> {

	/**
	 * Retrieves the Meta-Data from the Meta-Data property.
	 * 
	 * @return The Meta-Data stored by the Meta-Data property.
	 */
	MD getMetaData();

	/**
	 * Provides a mutator for to a Meta-Data property.
	 * 
	 * @param <MD> The type of the Meta-Data property.
	 */
	public interface MetaDataMutator<MD> {

		/**
		 * Sets the Meta-Data for the Meta-Data property.
		 * 
		 * @param aMetaData The Meta-Data to be stored by the Meta-Data
		 *        property.
		 */
		void setMetaData( MD aMetaData );
	}

	/**
	 * Provides a builder method for a name property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <MD> The type of the Meta-Data property.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MetaDataBuilder<MD, B extends MetaDataBuilder<MD, B>> {

		/**
		 * Sets the Meta-Data for the Meta-Data property.
		 * 
		 * @param aMetaData The Meta-Data to be stored by the Meta-Data
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMetaData( MD aMetaData );
	}

	/**
	 * Provides a Meta-Data property.
	 * 
	 * @param <MD> The type of the Meta-Data property.
	 */
	public interface MetaDataProperty<MD> extends MetaDataAccessor<MD>, MetaDataMutator<MD> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setMetaData(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aMetaData The value to set (via {@link #setMetaData(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default MD letMetaData( MD aMetaData ) {
			setMetaData( aMetaData );
			return aMetaData;
		}
	}
}
