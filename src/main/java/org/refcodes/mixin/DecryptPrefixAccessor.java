// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a Decrypt-Prefix property.
 */
public interface DecryptPrefixAccessor {

	/**
	 * Retrieves the Decrypt-Prefix from the Decrypt-Prefix property.
	 * 
	 * @return The Decrypt-Prefix stored by the Decrypt-Prefix property.
	 */
	String getDecryptPrefix();

	/**
	 * Provides a mutator for a Decrypt-Prefix property.
	 */
	public interface DecryptPrefixMutator {

		/**
		 * Sets the Decrypt-Prefix for the Decrypt-Prefix property.
		 * 
		 * @param aDecryptPrefix The Decrypt-Prefix to be stored by the
		 *        Decrypt-Prefix property.
		 */
		void setDecryptPrefix( String aDecryptPrefix );
	}

	/**
	 * Provides a builder method for a Decrypt-Prefix property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DecryptPrefixBuilder<B extends DecryptPrefixBuilder<B>> {

		/**
		 * Sets the Decrypt-Prefix for the Decrypt-Prefix property.
		 * 
		 * @param aDecryptPrefix The Decrypt-Prefix to be stored by the
		 *        Decrypt-Prefix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDecryptPrefix( String aDecryptPrefix );
	}

	/**
	 * Provides a Decrypt-Prefix property.
	 */
	public interface DecryptPrefixProperty extends DecryptPrefixAccessor, DecryptPrefixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setDecryptPrefix(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aDecryptPrefix The {@link String} to set (via
		 *        {@link #setDecryptPrefix(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letDecryptPrefix( String aDecryptPrefix ) {
			setDecryptPrefix( aDecryptPrefix );
			return aDecryptPrefix;
		}
	}
}
