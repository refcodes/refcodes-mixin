// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classdomain ("http://www.gnu.org/software/classdomain/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a domain property.
 */
public interface DomainAccessor {

	/**
	 * Retrieves the domain from the domain property.
	 * 
	 * @return The domain stored by the domain property.
	 */
	String getDomain();

	/**
	 * Provides a mutator for a domain property.
	 */
	public interface DomainMutator {

		/**
		 * Sets the domain for the domain property.
		 * 
		 * @param aDomain The domain to be stored by the domain property.
		 */
		void setDomain( String aDomain );
	}

	/**
	 * Provides a mutator for an domain property.
	 * 
	 * @param <B> The builder which implements the {@link DomainBuilder}.
	 */
	public interface DomainBuilder<B extends DomainBuilder<?>> {

		/**
		 * Sets the domain to use and returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aDomain The domain to be stored by the domain property.
		 * 
		 * @return This {@link DomainBuilder} instance to continue
		 *         configuration.
		 */
		B withDomain( String aDomain );
	}

	/**
	 * Provides a domain property.
	 */
	public interface DomainProperty extends DomainAccessor, DomainMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setDomain(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aDomain The {@link String} to set (via
		 *        {@link #setDomain(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letDomain( String aDomain ) {
			setDomain( aDomain );
			return aDomain;
		}
	}
}
