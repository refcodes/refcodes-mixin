// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

import java.io.PrintStream;

/**
 * Provides an accessor for an standard {@link PrintStream} property.
 */
public interface StandardPrintStreamAccessor {

	/**
	 * Retrieves the standard {@link PrintStream} from the standard
	 * {@link PrintStream} property.
	 * 
	 * @return The standard {@link PrintStream} stored by the standard
	 *         {@link PrintStream} property.
	 */
	PrintStream getStandardPrintStream();

	/**
	 * Provides a mutator for an standard {@link PrintStream} property.
	 */
	public interface StandardPrintStreamMutator {

		/**
		 * Sets the standard {@link PrintStream} for the standard
		 * {@link PrintStream} property.
		 * 
		 * @param aStandardPrintStream The standard {@link PrintStream} to be
		 *        stored by the standard {@link PrintStream} property.
		 */
		void setStandardPrintStream( PrintStream aStandardPrintStream );
	}

	/**
	 * Provides a mutator for an standard {@link PrintStream} property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link StandardPrintStreamBuilder} .
	 */
	public interface StandardPrintStreamBuilder<B extends StandardPrintStreamBuilder<?>> {

		/**
		 * Sets the standard {@link PrintStream} to use and returns this builder
		 * as of the Builder-Pattern.
		 * 
		 * @param aStandardPrintStream The standard {@link PrintStream} to be
		 *        stored by the standard {@link PrintStream} property.
		 * 
		 * @return This {@link StandardPrintStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withStandardPrintStream( PrintStream aStandardPrintStream );
	}

	/**
	 * Provides an standard {@link PrintStream} property.
	 */
	public interface StandardPrintStreamProperty extends StandardPrintStreamAccessor, StandardPrintStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link PrintStream}
		 * (setter) as of {@link #setStandardPrintStream(PrintStream)} and
		 * returns the very same value (getter).
		 * 
		 * @param aStandardPrintStream The {@link PrintStream} to set (via
		 *        {@link #setStandardPrintStream(PrintStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default PrintStream letStandardPrintStream( PrintStream aStandardPrintStream ) {
			setStandardPrintStream( aStandardPrintStream );
			return aStandardPrintStream;
		}
	}
}
