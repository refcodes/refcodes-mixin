// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a delimiters property.
 */
public interface DelimitersAccessor {

	/**
	 * Retrieves the delimiters from the delimiters property.
	 * 
	 * @return The delimiters stored by the delimiters property.
	 */
	char[] getDelimiters();

	/**
	 * Provides a mutator for a delimiters property.
	 */
	public interface DelimitersMutator {

		/**
		 * Sets the delimiters for the delimiters property.
		 * 
		 * @param aDelimiters The delimiters to be stored by the delimiters
		 *        property.
		 */
		void setDelimiters( char... aDelimiters );
	}

	/**
	 * Provides a builder method for a delimiters property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DelimitersBuilder<B extends DelimitersBuilder<B>> {

		/**
		 * Sets the delimiters for the delimiters property.
		 * 
		 * @param aDelimiters The delimiters to be stored by the delimiters
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDelimiters( char... aDelimiters );
	}

	/**
	 * Provides a delimiters property.
	 */
	public interface DelimitersProperty extends DelimitersAccessor, DelimitersMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link DelimitersMutator#setDelimiters(char...)} and returns the very
		 * same value (getter).
		 * 
		 * @param aDelimiters The character to set (via
		 *        {@link DelimitersMutator#setDelimiters(char...)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default char[] letDelimiters( char... aDelimiters ) {
			setDelimiters( aDelimiters );
			return aDelimiters;
		}
	}
}
