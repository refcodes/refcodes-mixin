// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a row height property.
 */
public interface RowHeightAccessor {

	/**
	 * Retrieves the row height from the row height property.
	 * 
	 * @return The row height stored by the row height property.
	 */
	int getRowHeight();

	/**
	 * Provides a mutator for a row height property.
	 */
	public interface RowHeightMutator {

		/**
		 * Sets the row height for the row height property.
		 * 
		 * @param aRowHeight The row height to be stored by the row height
		 *        property.
		 */
		void setRowHeight( int aRowHeight );
	}

	/**
	 * Provides a builder method for a row height property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RowHeightBuilder<B extends RowHeightBuilder<B>> {

		/**
		 * Sets the row height for the row height property.
		 * 
		 * @param aRowHeight The row height to be stored by the row height
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRowHeight( int aRowHeight );
	}

	/**
	 * Provides a row height property.
	 */
	public interface RowHeightProperty extends RowHeightAccessor, RowHeightMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setRowHeight(int)} and returns the very same value (getter).
		 * 
		 * @param aRowHeight The integer to set (via
		 *        {@link #setRowHeight(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letRowHeight( int aRowHeight ) {
			setRowHeight( aRowHeight );
			return aRowHeight;
		}
	}
}
