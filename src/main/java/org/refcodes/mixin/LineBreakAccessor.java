// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a line break property.
 */
public interface LineBreakAccessor {

	/**
	 * Retrieves the line break from the line break property.
	 * 
	 * @return The line break stored by the line break property.
	 */
	String getLineBreak();

	/**
	 * Provides a mutator for a line break property.
	 */
	public interface LineBreakMutator {

		/**
		 * Sets the line break for the line break property.
		 * 
		 * @param aLineBreak The line break to be stored by the line break
		 *        property.
		 */
		void setLineBreak( String aLineBreak );
	}

	/**
	 * Provides a builder method for a line break property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LineBreakBuilder<B extends LineBreakBuilder<B>> {

		/**
		 * Sets the line break for the line break property.
		 * 
		 * @param aLineBreak The line break to be stored by the line break
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLineBreak( String aLineBreak );
	}

	/**
	 * Provides a line break property.
	 */
	public interface LineBreakProperty extends LineBreakAccessor, LineBreakMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setLineBreak(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aLineBreak The {@link String} to set (via
		 *        {@link #setLineBreak(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letLineBreak( String aLineBreak ) {
			setLineBreak( aLineBreak );
			return aLineBreak;
		}
	}
}
