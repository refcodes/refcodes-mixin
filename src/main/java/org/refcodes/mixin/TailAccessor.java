// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.mixin;

/**
 * Provides an accessor for a tail property.
 *
 * @param <T> The type of the tail to be used.
 */
public interface TailAccessor<T> {

	/**
	 * Retrieves the value from the tail property.
	 * 
	 * @return The tail stored by the tail property.
	 */
	T getTail();

	/**
	 * Provides a mutator for a tail property.
	 * 
	 * @param <T> The type of the tail property.
	 */
	public interface TailMutator<T> {

		/**
		 * Sets the tail for the tail property.
		 * 
		 * @param aTail The tail to be stored by the tail property.
		 */
		void setTail( T aTail );
	}

	/**
	 * Provides a builder method for a tail property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <T> the tail type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TailBuilder<T, B extends TailBuilder<T, B>> {

		/**
		 * Sets the tail for the tail property.
		 * 
		 * @param aTail The tail to be stored by the tail property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTail( T aTail );
	}

	/**
	 * Provides a tail property.
	 * 
	 * @param <T> The type of the tail property.
	 */
	public interface TailProperty<T> extends TailAccessor<T>, TailMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setTail(Object)} and returns the very same value (getter).
		 * 
		 * @param aTail The value to set (via {@link #setTail(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letTail( T aTail ) {
			setTail( aTail );
			return aTail;
		}
	}
}
